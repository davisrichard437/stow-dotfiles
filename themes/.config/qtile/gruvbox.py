# dracula.py
# colors resource for dracula theme
colors = {
    "foreground": "ebdbb2",
    "background": "282828",
    "focus": "98971a",
    "focus2": "98971a",
    "urgent": "cc241d",
}
