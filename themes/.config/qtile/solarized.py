# solarized.py
# colors resource for dracula theme

colors = {
    "foreground": "93a1a1",
    "background": "002b36",
    "focus": "859900",
    "focus2": "859900",
    "urgent": "dc322f",
}
