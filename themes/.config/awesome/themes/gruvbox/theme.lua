-- ~/.config/awesome/themes/dracula/theme.lua
-- dracula theme for the awesome wm

local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local dpi   = require("beautiful.xresources").apply_dpi

local os = os
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility

local theme                                     = {}
theme.confdir                                   = os.getenv("HOME") .. "/.config/awesome/themes/dracula"
theme.font                                      = "Hack Nerd Font 12"
theme.taglist_font                              = "Hack Nerd Font 12"
theme.menu_bg_normal                            = "#282828"
theme.menu_bg_focus                             = "#98971a"
theme.bg_normal                                 = "#282828"
theme.bg_focus                                  = "#98971a"
theme.bg_urgent                                 = "#cc241d"
theme.fg_normal                                 = "#ebdbb2"
theme.fg_focus                                  = "#282828"
theme.fg_urgent                                 = "#ebdbb2"
theme.fg_minimize                               = "#ebdbb2"
theme.border_width                              = dpi(2)
theme.border_normal                             = "#282828"
theme.border_focus                              = "#98971a"
theme.border_marked                             = "#3ca4d8"
theme.menu_border_width                         = 0
theme.menu_height                               = dpi(25)
theme.menu_width                                = dpi(260)
theme.menu_submenu_icon                         = theme.confdir .. "/icons/submenu.png"
theme.menu_fg_normal                            = "#aaaaaa"
theme.menu_fg_focus                             = "#ff8c00"
theme.menu_bg_normal                            = "#050505dd"
theme.menu_bg_focus                             = "#050505dd"
theme.tasklist_plain_task_name                  = true
theme.tasklist_disable_icon                     = true
theme.useless_gap                               = 6
theme.layout_tile                               = theme.confdir .. "/icons/tile.png"
theme.layout_tilebottom                         = theme.confdir .. "/icons/tilebottom.png"
theme.layout_max                                = theme.confdir .. "/icons/max.png"
theme.layout_fullscreen                         = theme.confdir .. "/icons/fullscreen.png"
theme.layout_magnifier                          = theme.confdir .. "/icons/magnifier.png"
theme.layout_floating                           = theme.confdir .. "/icons/floating.png"

local markup = lain.util.markup

-- Textclock
os.setlocale(os.getenv("LANG")) -- to localize the clock
local mytextclock = wibox.widget.textclock(markup("#ebdbb2", "%a, %d %b %Y, %H:%M "))
mytextclock.font = theme.font

-- Calendar
theme.cal = lain.widget.cal({
    attach_to = { mytextclock },
    notification_preset = {
        font = "Hack Nerd Font 12",
        fg   = theme.fg_normal,
        bg   = theme.bg_normal
    }
})

-- Weather
theme.weather = lain.widget.weather({
    APPID = "d1415963f44003abb1d698cabb9c8211",
    city_id = 6077243, -- https://openweathermap.org/find?q=
    notification_preset = { font = "Hack Nerd Font 12", fg = theme.fg_normal },
    weather_na_markup = markup.fontfg(theme.font, "#ebdbb2", "| N/A |"),
    settings = function()
        descr = weather_now["weather"][1]["description"]:lower()
        units = math.floor(weather_now["main"]["temp"])
        widget:set_markup(markup.fontfg(theme.font, "#ebdbb2", "| " .. descr .. " " .. units .. "°C |"))
    end
})

function theme.at_screen_connect(s)
    -- Quake application
   -- s.quake = lain.util.quake({ app = awful.util.terminal })
   s.quake = lain.util.quake({ app = "alacritty", height = 0.50, argname = "--name %s" })

    -- If wallpaper is a function, call it with the screen
    local wallpaper = theme.wallpaper
    if type(wallpaper) == "function" then
        wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, true)

    -- Tags
    awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(my_table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, awful.util.tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s, height = dpi(20), bg = theme.bg_normal, fg = theme.fg_normal })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        spacing = 6,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            spacing = 6,
            s.mylayoutbox,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            spacing = 6,
            wibox.widget.systray(),
            theme.weather.widget,
            mytextclock,
        },
    }
end

return theme
