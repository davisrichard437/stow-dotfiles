#!/usr/bin/env bash
# extra/install.sh
# Script to install theming-related configuration

mkdir -p ~/.themes/
if ! ls /usr/share/icons/ ~/.icons 2>/dev/null | grep -w "Tela-brown-dark" ; then
    git clone https://github.com/vinceliuice/Tela-icon-theme.git
    if ./Tela-icon-theme/install.sh brown ; then
        echo "Tela icon theme installed successfully."
    else
        echo "Failed to install tela-brown icon theme."
    fi
fi
if ! ls /usr/share/icons/ ~/.icons 2>/dev/null | grep -w "Tela-manjaro-dark" ; then
    git clone https://github.com/vinceliuice/Tela-icon-theme.git
    if ./Tela-icon-theme/install.sh manjaro ; then
        echo "Tela icon theme installed successfully."
    else
        echo "Failed to install tela-manjaro icon theme."
    fi
fi
rm -rf Tela-icon-theme
