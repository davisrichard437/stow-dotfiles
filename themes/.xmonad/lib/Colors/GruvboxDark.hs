module Colors.GruvboxDark where

import XMonad

colorBack = "#282828"
colorFore = "#ebdbb2"
colorFocus = "#98971a"
colorUrgent = "#cc241d"
