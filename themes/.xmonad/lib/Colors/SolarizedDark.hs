module Colors.SolarizedDark where

import XMonad

colorBack = "#002b36"
colorFore = "#839496"
colorFocus = "#859900"
colorUrgent = "#dc322f"
