# Themes

These are the configuration files for additional themes and related functionality.

## Features

Includes the following themes in addition to [dracula](https://draculatheme.com/), the standard for this configuration:

- [Gruvbox](https://github.com/morhetz/gruvbox)
- [Solarized Dark](https://ethanschoonover.com/solarized/)

Includes theming for the following programs/features:

- Wallpaper
- Alacritty
- Dunst
- Emacs
- GTK 2.0, 3.0
- Gvim
- Icons
- Rofi
- Window Managers
  - Awesome
  - openbox
    - tint2
  - Qtile
  - spectrwm
  - XMonad
    - XMobar

Also includes a rofi script `~/.local/bin/color-scheme` to choose between any of the three available themes.
