#!/usr/bin/env bash
# ~/.config/spectrwm/baraction.sh
# script to populate the spectrwm status bar

# Volume
vol() {
    vol=$(amixer sget Master | awk 'FNR == 6 {print $(NF-1)}' | sed -e 's/\[//' -e 's/\]//')
    if [ -z "$vol" ] ; then
        echo "Volume: N/A | "
    else
        echo -e "Volume: $vol | "
    fi
}

# Weather
wttr() {
    tempC=$(curl -s wttr.in\?m0TQ | awk '/°C/ {print $(NF-1)}' | sed -e 's/+//g' -e 's/([0-9]*)//g')
    loc=$(curl -s wttr.in\?m0Tq | awk 'FNR == 1 {print $1}' | sed 's/,//')
    if [ -z "$tempC" ] ; then
        echo "Weather: N/A | "
    else
        echo "$loc: $tempC °C | "
    fi
}

# Ping
png() {
    png=$(ping -c 1 google.com | awk 'FNR == 2 {print $(NF-1)}' | cut -d '=' -f2)
    if [ -z "$png" ] ; then
	echo "Not connected | "
    else
	echo "Ping: ${png%.*} ms | "
    fi
}

# Updates
upd() {
    upd=$(checkupdates | wc -l)
    if [ $upd -eq 0 ] ; then
        echo ""
    elif [ $upd -eq 1 ] ; then
	echo "$upd Update! | "
	: '
	if [ -z "$notified" ]  ; then
	    action=$(dunstify --action="default,Pamac" "$upd update available")
		notified="true"
		if [ "$action" == "default" ] ; then
		    pamac-manager &
			notified=""
		fi
	fi
	'
    else
	echo "$upd Updates! | "
	: '
	if [ -z "$notified" ]  ; then
	    action=$(dunstify --action="default,Pamac" "$upd updates available")
		notified="true"
		if [ "$action" == "default" ] ; then
		    pamac-manager &
			notified=""
		fi
	fi
	'
    fi
}

# Battery
bat() {
    bat=$(acpi 2> /dev/null | cut -d ',' -f1-2 | awk '{print $NF}')
    #bat=$(acpi | cut -d ',' -f1-2 | awk '{for(i=3;i<=NF;i++) printf $i" "; print ""}')
    if [ -z "$bat" ] ; then
	echo ""
    else
	echo "Battery: $bat | "
    fi
}

min=0
while :; do
    if [ $min -eq 60 ] ; then
	min=0
    fi
    if [ $min -eq 0 ] ; then
	weather=$(wttr)
    fi
    echo "$(upd)$(bat)$(vol)$(png)$weather"
    min=$(($min+1))
    sleep 1m
done
