#!/usr/bin/env bash
# ~/.config/spectrwm/keys.sh
# Script to list keybindings

sed -n '/STARTKEYS/,/ENDKEYS/p' ~/.spectrwm.conf | \
    sed -e '/program/d' \
	-e '/^$/d' \
	-e 's/^# KBGROUP /\n/g' \
	-e '/^#/d' \
	-e 's/bind\[//g' \
        -e 's/\]\s*=\s*/:\t/g' | \
    yad --fontname=Ubuntu\ Mono\ 12 --title Keybindings --no-buttons --text-info
