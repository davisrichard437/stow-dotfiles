# spectrwm

These are the files required for the spectrwm configuration.

## Dependencies

- alacritty
- amixer
- audacity
- brightnessctl
- curl
- dunst
- feh
- firefox
- geoclue-2.0
- gmrun
- gnome-screenshot
- ibus-daemon
- light-locker
- musescore
- noto-fonts-cjk
- pacman-contrib
- parcellite
- pavucontrol
- pcmanfm
- picom-ibhagwan-git
- polkit-gnome
- reaper
- rofi
- gvim
- vlc
- vmpk

## Files

- `~/.spectrwm.conf`: the main config file for the spectrwm window manager.
- `~/.config/spectrwm/baraction.sh`: script to populate the status bar.
- `~/.config/spectrwm/keys.sh`: script to list keybindings.

## Screenshot

![spectrwm-dracula](spectrwm/screenshot.png)
