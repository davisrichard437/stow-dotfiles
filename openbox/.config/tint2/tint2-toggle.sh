#!/usr/bin/env bash
# ~/.config/tint2/tint2-toggle.sh
# script to toggle tint2

function run {
    if ! pgrep $1 ; then
        $@&
    fi
}

if [ $(ps -aux | grep tint2 | sed -e '/grep/d' -e '/toggle/d' | wc -l) -gt 0 ] ; then
    pkill -9 tint2
else
    run ibus-daemon -drxR
    run nm-applet
    run pamac-tray
    run parcellite
    ~/.config/powerkit/power-management.sh
    tint2 -c ~/.config/tint2/dracula-tint2rc &
fi
