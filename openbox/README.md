# Openbox

These are the files required for the openbox configuration.

## Dependencies

- alacritty
- archlinux-xdg-desktop
- audacity
- brightnessctl
- dunst
- emacs
- feh
- firefox
- gnome-screenshot
- ibus-daemon
- light-locker
- lxappearance-obconf
- musescore
- network-manager-applet
- numlockx
- obconf
- openbox
- parcellite
- pavucontrol
- pcmanfm
- picom-ibhagwan-git
- polkit-gnome
- redshift
- rofi
- tint2
- vlc
- vmpk
- xdotool

## Files

- `~/.config/openbox/autostart`: script to start necessary programs.
- `~/.config/openbox/menu.xml`: right-click menu.
- `~/.config/openbox/rc.xml`: the main config file for the Openbox window manager.
- `~/.config/tint2/tint2rc`: the config file for the tint2 panel.
- `~/.config/tint2/tint2-toggle.sh`: script to toggle the tint2 panel.
- `~/.local/share/applications/open-openbox-menu.desktop`: desktop entry used to add openbox menu to tint2 panel.

## Screenshot

![openbox-dracula](openbox/screenshot.png)
