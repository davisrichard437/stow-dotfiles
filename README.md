# Stow Dotfiles

## Intro

The following are dotfiles for an Arch Linux based desktop. The dotfiles are packaged according to window manager and are distributed using GNU Stow.

## Usage

### Base Install

Steps for base install not mentioned in the [Arch Wiki](https://wiki.archlinux.org/title/Installation_guide):

#### Install Packages

    # curl -s https://gitlab.com/davisrichard437/stow-dotfiles/-/raw/main/base.txt | pacstrap /mnt -

#### Network Configuration

Configure [`/etc/hosts`](https://wiki.archlinux.org/title/Network_configuration#Local_hostname_resolution)

Enable NetworkManager:

    # systemctl enable NetworkManager
    
#### User Priveleges

Add an unprivileged user and set their password:

```
# useradd -m -G audio,video,optical,storage,floppy,wheel,lp -s /usr/bin/zsh -c "Foo Bar" foo
# passwd foo
```
    
Enable wheel group user privileges:

    # visudo
    
#### Install GRUB

For a BIOS system:

    # grub-install /dev/sda

Or for an EFI system:

    # grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB
    
Generate grub config:

    # grub-mkconfig -o /boot/grub/grub.cfg

#### Install Microcode

    # pacman -S (intel/amd)-ucode
    
### Desktop Install

After installation, clone the repo:

```
$ git clone https://gitlab.com/davisrichard437/stow-dotfiles.git
$ cd stow-dotfiles
```

For the development branch, do the following instead:

```
$ git clone -b latest https://gitlab.com/davisrichard437/stow-dotfiles/stow-dotfiles.git
$ cd stow-dotfiles
```

#### Install using install.sh script (recommended)

Install can either be done all in one step:

    $ ./install.sh

This will install dependencies and configurations for all packages.
    
Or in individual steps:

- `./install.sh file`: Create necessary directories and backup previous configurations.
  - Prepares the filesystem by moving any conflicting files to `~/.backup`, allowing GNU Stow to run without error while keeping one older version of each config.
  - Mirrors the directory structure of the configuration package in the home directory, ensuring that unwanted files (e.g. compile results) don't populate the git repository.
- `./install.sh pkg`: Install necessary packages.
  - Enables parallel downloads.
  - Installs necessary packages according to two package lists: `pacman.txt` (main repositories) and `aur.txt` (AUR packages).
  - Automatically installs paru if not already present.
  - Note: Invocations of all package managers use the `--noconfirm` option, so any conflicts will have to be resolved manually.
  - Note: This step will prompt a user for a sudo password throughout the process.
- `./install.sh config`: Install desktop configuration.
  - Creates userspace directories (`~/Desktop/`, `~/Documents/`, etc).
  - Sets some default desktop applications.
  - Installs system-wide configurations.
  - Installs configurations for the selected package(s).
  - Initializes emacs to avoid overly long startup time.
  - Note: This step will prompt a user for a sudo password throughout the process.

The `file`, `pkg`, and `config` commands may be followed by a list of packages the user wishes to install, for example:

```
$ ./install.sh file general qtile
$ ./install.sh pkg general qtile
$ ./install.sh config general qtile
```

You may also choose to install the dependencies and configurations for a list of packages, for example:

    $ ./install.sh general qtile

For more details, see the following help commands:

```
$ ./install.sh help
$ ./install.sh file help
$ ./install.sh pkg help
$ ./install.sh config help
```

For troubleshooting/output logging, do the following:

    $ ./install.sh (args) >> log.txt

#### Install manually (alternative)

Install necessary packages:

```
$ sudo pacman -S --needed - < pkglists/pacman.txt
$ paru -S - < pkglists/aur.txt
```

Each package has its own pkglists, for example:

```
$ sudo pacman -S --needed - < general/pacman.txt
$ paru -S - < general/aur.txt
```

Stow the configuration package for stow:

    $ stow -t ~ stow

Remove conflicting files:

    $ rm ~/.bashrc

(Recommended) Make directories that will be populated by their programs:

    $ mkdir -p ~/.config/qtile ~/.emacs.d ~/.xmonad

Stow the desktop:

    $ stow general awesome qtile spectrwm xmonad

## Packages

The following are the packages available to be stowed. Each corresponds to a single component of a system.

- `general`: these are the configuration files for programs used by two or more of the other packages. These are optional if all of these configuration files are already present.
- `extra`: these are the configuration files not necessary for base system functionality. The pkglists also provide some potentially desirable additional applications.
- `themes`: these are the configuration files for additional themes, along with a theme switching script.
- `awesome`: these are the configuration files for the Awesome window manager.
- `qtile`: these are the configuration files for the Qtile window manager.
- `openbox`: these are the configuration files for the Openbox window manager.
- `spectrwm`: these are the configuration files for the spectrwm window manager.
- `xmonad`: these are the configuration files for the XMonad window manager.

In addition to these stow packages, this repository includes the following relevant files:

- `.gitignore`: list of files to be ignored by git. Currently only includes lockfiles for `install.sh`.
- `install.sh`: script to install desktop configuration files.
- `LICENSE`: GNU GPL v3.0+.
- `base.txt`: list of packages comprising the base system.
