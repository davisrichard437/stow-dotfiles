# Awesome WM

These are the files required for the Awesome WM configuration.

## Dependencies

- alacritty
- audacity
- awesome
- brightnessctl
- dunst
- emacs
- feh
- firefox
- geoclue-2.0
- gmrun
- gnome-screenshot
- ibus-daemon
- light-locker
- musescore
- pamac-tray
- parcellite
- pavucontrol
- pcmanfm
- picom-ibhagwan-git
- polkit-gnome
- reaper
- rofi
- ttf-ubuntu-font-family
- gvim
- vlc
- vmpk
- volumeicon

## Files

- `~/.config/awesome/rc.lua`: the main config file for the awesome wm
- `~/.config/awesome/themes/dracula/theme.lua`: dracula theme for the awesome wm
- `~/.config/awesome/themes/dracula/icons/*`: layout icons

## Screenshot

![awesome-dracula](awesome/screenshot.png)
