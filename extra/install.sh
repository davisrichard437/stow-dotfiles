#!/usr/bin/env bash
# extra/install.sh
# Script to install extra configuration

# npm install -g bash-language-server
# npm install -g pyright
mkdir -p ~/.local/bin
wget -O ~/.local/bin/ani-cli https://raw.githubusercontent.com/pystardust/ani-cli/master/ani-cli
chmod +x ~/.local/bin/ani-cli
