# Extra

These are the configuration files not necessary for base system functionality. The pkglists also provide some potentially desirable additional applications.

## Features

### Music management with `yt-music`:

#### Files:

- `~/Music/yt-music.sh`: script to manage subdirectories of ~/Music/ with youtube-dl.
- `~/Music/*.txt`: backup text files of my personal playlist, with filename indicating genre.

#### Dependencies:

- youtube-dl

#### Usage:

After installing the `extra` package, the following symlinks will be added to the `~/Music/` directory:

```
Contemporary.txt
Country.txt
JPop.txt
```

The `.txt` files are lists of youtube codes all belonging to the genre described in the filename. To download all of the music in a single genre, execute the following script (symlinked to `~/.local/bin/yt-music`)

    $ yt-music download (genre).txt

This will create the directory `~/Music/(genre)` and populate it with `.wav` audio files corresponding to the codes listed in `(genre).txt`.

Should the user use `youtube-dl` to manually add music to these folders, or create their own folder, `yt-music.sh` may be used to generate text files that reflect these changes. To do so, do the following:

    $ yt-music backup ~/Music/(genre)/

This will create `~/Music/(genre).txt`, formatted exactly as described above, with a list of URLs corresponding exactly to the contents of `~/Music/(genre)/`.

These genre-based directories make music playback control very easy in emacs. Use either emms-(add/play)-directory, or "SPC m (a/p) d" in normal or visual mode to change the current playlist, and "emms-ffwd" or "SPC m l" to begin playback.
