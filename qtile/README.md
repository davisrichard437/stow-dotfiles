# Qtile

These are the files required for the qtile configuration.

## Dependencies

- alacritty
- audacity
- brightnessctl
- dunst
- emacs
- feh
- firefox
- geoclue-2.0
- gmrun
- gnome-screenshot
- ibus-daemon
- light-locker
- musescore
- pamac-tray
- parcellite
- pavucontrol
- pcmanfm
- picom-ibhagwan-git
- polkit-gnome
- python-pyxdg
- qtile
- rofi
- gvim
- vlc
- vmpk
- volumeicon

## Files

- `~/.config/qtile/config.py`: the main config file for the Qtile window manager.
- `~/.config/qtile/scripts/autostart.sh`: script to start necessary programs.
- `~/.config/qtile/scripts/keys.sh`: script to list keybindings.

## Screenshot

![qtile-dracula](qtile/screenshot.png)
