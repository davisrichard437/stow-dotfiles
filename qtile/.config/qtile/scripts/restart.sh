#!/usr/bin/env bash
# ~/.config/qtile/scripts/restart.sh
# Script to restart qtile

qtile cmd-obj -o cmd -f restart
