#!/usr/bin/env bash
# ~/.config/qtile/scripts/autostart.sh
# script to start necessary programs with Qtile

function run {
    if ! pgrep $1 ;
    then
        $@&
    fi
}

run /usr/lib/geoclue-2.0/demos/agent
run /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
# run picom --experimental-backends --config ~/.config/picom/picom.conf
# run light-locker --lock-on-suspend --lock-on-lid --lock-after-screensaver 60
# run dunst -config ~/.config/dunst/dunstrc
~/.config/dunst/start-dunst.sh
run emacs --daemon
run ibus-daemon -drxR
run nm-applet
run pamac-tray
run parcellite
run redshift
~/.config/powerkit/power-management.sh
~/.fehbg
