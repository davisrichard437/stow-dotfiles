#!/usr/bin/env bash
# ~/.config/qtile/scripts/keys.sh
# script to list keybindings

sed -n '/STARTKEYS/,/ENDKEYS/p' ~/.config/qtile/config.py | \
    sed -e 's/[ \t]*# KBGROUP /\n/g' \
    -e '/^[ \t]#/d' \
    -e '/#/d' \
    -e 's/[ \t]*Key(//g' \
    -e '/"[a-zA-Z]*"\]/s/",/":\t/g' \
    -e '/"[a-zA-Z]*"\]/!s/",/":\t\t/g' \
    -e 's/\[\], //g' \
    -e '/^$/d' \
    -e 's/),$//g' \
    -e 's/lazy.*desc=//g' | \
yad --fontname=Ubuntu\ Mono\ 12 --title Keybindings --no-buttons --text-info
