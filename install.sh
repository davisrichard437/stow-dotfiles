#!/usr/bin/env bash
# install.sh
# Script to install desktop configuration files

# Get absolute path of binary
# in case cloned to different
# directory that stow-dotfiles
rel_path=$(dirname "$0")
abs_path="$(builtin cd $rel_path && pwd)"

# General help text
usage() {
    cat <<EOF
install.sh
Script to install desktop configuration files:
Will prompt user for a sudo/root password throughout the process.
Usage:
  $ ./install.sh [option]
Options:
  file:      prepares the filesystem
             see ./install.sh file help for more information
  pkg:       installs necessary desktop packages
             see ./install.sh pkg help for more information
  config:    installs desktop config files
             see ./install.sh config help for more information
  no option: runs all of the above
  (package): runs file, pkg, and config for (general) and (package)
  help:      display this help
EOF
}

# Function to prepare the filesystem
file() {
    cd $abs_path
    mkdir -p ~/.backup
    # If no arguments are passed, prepare filesystem for all packages.
    if [ "$1" == "help" ] ; then
        cat <<EOF
install.sh file
Create necessary directories and backup previous configurations.
Usage:
  $ ./install.sh file (package)
Packages:
  general:   general filesystem
  extra:     extra filesystem
  awesome:   awesome wm filesystem
  openbox:   openbox filesystem
  qtile:     qtile filesystem
  spectrwm:  spectrwm filesystem
  themes:    themes filesystem
  xmonad:    xmonad filesystem
  no option: all filesystems
  help:      display this help
EOF
        exit 0
    elif [ -z $1 ] ; then
        echo "Preparing filesystem."
        # Makes necessary directories so stow doesn't symlink to the local git repo
        find * -type d | sed '/\//!d' | cut -d '/' -f2- | sort | uniq -u | xargs -I {} mkdir -p ~/{}
        find * -type d | sed '/\//!d' | cut -d '/' -f2- | sort | uniq -u | xargs -I {} mkdir -p ~/.backup/{}
        # Moves all potentially conflicting files to ~/.backup
        find * -type f | \
            sed -e '/\//!d' \
                -e '/README.md/d' \
                -e '/screenshot.png/d' \
                -e '/pkglists/d' \
                -e '/pacman.txt/d' \
                -e '/aur.txt/d' | \
            cut -d '/' -f2- | \
            xargs -I {} sh -c 'if [ -f ~/{} ] ; then mv ~/{} ~/.backup/{} ; fi'
    else
        for i in $@ ; do
            # Prepare filesystem for single package
            if [ -d $i ] ; then
                echo "Preparing $i filesystem."
                # Makes necessary directories so stow doesn't symlink to the local git repo
                find $i -type d | sed '/\//!d' | cut -d '/' -f2- | sort | uniq -u | xargs -I {} mkdir -p ~/{}
                find $i -type d | sed '/\//!d' | cut -d '/' -f2- | sort | uniq -u | xargs -I {} mkdir -p ~/.backup/{}
                # Moves all potentially conflicting files to ~/.backup
                find $i -type f | \
                    sed -e '/\//!d' \
                        -e '/README.md/d' \
                        -e '/screenshot.png/d' \
                        -e '/pacman.txt/d' \
                        -e '/aur.txt/d' | \
                    cut -d '/' -f2- | \
                    xargs -I {} sh -c 'if [ -f ~/{} ] ; then mv ~/{} ~/.backup/{} ; fi'
                shift
            else
                echo "No such package $i."
                exit 1
            fi
        done
    fi
}

# Function to install only necessary AUR packages
aur() {
    # Install paru if not already installed
    if ! pacman -Q paru 2>/dev/null ; then
        echo "Paru not found, installing."
        git clone https://aur.archlinux.org/paru-bin.git
        cd paru-bin
        if makepkg --nocolor --noconfirm -si ; then
            echo "Paru installation successful."
            cd ..
            rm -rf paru-bin
        else
            echo "Paru installation failed."
            cd ..
            rm -rf paru-bin
            exit 1
        fi
    fi
    if ! paru -S --color never --needed --noconfirm $@ ; then
        exit 1
    fi
}

# Function to install necessary packages
pkg() {
    cd $abs_path
    # If no arguments are passed, install all relevant packages
    if [ "$1" == "help" ] ; then
        # pkg help text
        cat <<EOF
install.sh pkg
Install necessary packages.
Will prompt user for a sudo password throughout the process.
Usage:
  $ ./install.sh pkg (package)
Packages:
  general:   general dependencies
  extra:     extra dependencies
  awesome:   awesome wm dependencies
  openbox:   openbox dependencies
  qtile:     qtile dependencies
  spectrwm:  spectrwm dependencies
  themes:    themes dependencies
  xmonad:    xmonad dependencies
  no option: all dependencies
  help:      display this help
EOF
        exit 0
    elif [ -z $1 ] ; then
        echo "Installing packages."
        # Enable parallel downloads
        sudo sed -i 's/#ParallelDownloads/ParallelDownloads/' /etc/pacman.conf
        if ! sudo pacman -S --color never --needed --noconfirm $(cat */pacman.txt | sort -u) ; then
            echo "Installation of pacman packages failed; try again later."
            exit 1
        else
            if ! aur $(cat */aur.txt | sort -u) ; then
                echo "Installation of AUR packages failed; try again later."
                exit 1
            fi
        fi
    else
        # Enable parallel downloads
        sudo sed -i 's/#ParallelDownloads/ParallelDownloads/' /etc/pacman.conf
        for l in "$@" ; do
            # Install packages for a specific configuration package
            if [ -d $l ] ; then
                echo "Installing $l packages."
                if ! sudo pacman -S --color never --needed --noconfirm $(cat $l/pacman.txt) ; then
                    echo "Installation of $l pacman packages failed; try again later."
                    exit 1
                else
                    if ! aur $(cat $l/aur.txt) ; then
                        echo "Installation of $l AUR packages failed; try again later."
                        exit 1
                    else
                        shift
                    fi
                fi
            else
                echo "No such package $l."
                exit 1
            fi
        done
    fi
}

system_config() {
    # Create standard userspace directories
    if pacman -Q xdg-user-dirs ; then
        if [ ! -d ~/Documents ] ; then
            xdg-user-dirs-update
        fi
    else
        echo "Install general dependencies before running config."
        exit 1
    fi
    # Check for stow before continuing
    if ! pacman -Q stow ; then
        echo "Install general dependencies before running config."
        exit 1
    fi
    # Set desktop defaults
    if pacman -Q xdg-utils ; then
        xdg-settings set default-web-browser firefox.desktop
    fi
    # Install root configurations
    if ! ls /usr/share/icons/ ~/.icons 2>/dev/null | grep -w "Tela-purple-dark" ; then
        if git clone https://github.com/vinceliuice/Tela-icon-theme.git && \
        sudo ./Tela-icon-theme/install.sh purple && \
        rm -rf Tela-icon-theme ; then
            echo "Tela icon theme installed successfully."
        else
            echo "Failed to install tela icon theme."
            exit 1
        fi
    fi
    if [ ! -f .root ] ; then
        echo "Installing system-wide configurations."
        if sudo mkdir -p /usr/share/backgrounds && \
        sudo cp general/Wallpapers/mountain.png /usr/share/backgrounds/ && \
        sudo sed -i -e 's/#session-wrapper/session-wrapper/' \
                    -e '/#greeter-session/cgreeter-session=lightdm-gtk-greeter' /etc/lightdm/lightdm.conf && \
        sudo sed -i 's/#BottomUp/BottomUp/' /etc/paru.conf && \
        sudo sed -i -e 's/#Color/Color/' \
                    -e 's/#ParallelDownloads/ParallelDownloads/' \
                    -e 's/#VerbosePkgLists/VerbosePkgLists/' /etc/pacman.conf && \
        echo "Enter root (not sudo) password here to write /etc/lightdm/lightdm-gtk-greeter.conf:" && \
        su -c sh -c 'cat <<EOF > /etc/lightdm/lightdm-gtk-greeter.conf
[greeter]
theme-name = Dracula
background = /usr/share/backgrounds/mountain.png
icon-theme-name = Tela-purple-dark
user-background = false
font-name = Ubuntu 10
EOF' && \
        sudo systemctl enable lightdm.service && \
        sudo sed -i '/France,Germany/c--country US,Canada' /etc/xdg/reflector/reflector.conf && \
        sudo systemctl enable reflector.timer ; then
            touch .root
        else
            echo "Installing system-wide configurations failed; try again later."
            exit 1
        fi
    fi
}

start_emacs() {
    # Start emacs daemon to avoid updating settings on next boot
    if [ ! -f .emacs ] ; then
        if [ -e ~/.emacs.d/init.el ] && [ -L ~/.emacs.d/init.el ] ; then
            if pgrep emacs ; then
                killall emacs
            fi
            if pacman -Q emacs ; then
                if emacs --daemon ; then
                    touch .emacs
                else
                    echo "Emacs failed to start; try again later."
                    exit 1
                fi
            else
                echo "Emacs has not been installed. Ensure you install general packages before initializing emacs."
            fi
        else
            echo "Install and stow general configurations before starting emacs."
        fi
    else
        echo "Emacs has already been already initialized."
    fi
}

# Function to install configurations
config() {
    cd $abs_path
    # Install all configurations
    if [ "$1" == "help" ] ; then
        # config help text
        cat <<EOF
install.sh config
Install desktop configuration.
Will prompt user for a sudo/root password throughout the process.
Usage:
  $ ./install.sh config (package)
Packages:
  general:   general config files
  awesome:   awesome wm config files
  openbox:   openbox config files
  qtile:     qtile config files
  spectrwm:  spectrwm config files
  themes:    themes config files
  xmonad:    xmonad config files
  no option: all config files
  help:      display this help
EOF
        exit 0
    elif [ -z $1 ] ; then
        echo "Installing all configurations."
        if ! system_config ; then
            echo "Installing system configurations failed, please try again."
        fi
        if ! stow $(find . -maxdepth 1 -type d | sed -e '/.git/d' -e '/^.$/d' | cut -d '/' -f2) ; then
            echo "Stowing configurations failed; try again later."
            exit 1
        fi
        for o in * ; do
            if [ -f $o/install.sh ] ; then
                echo "Running $o post-install script."
                ./$o/install.sh
            fi
        done
        if ! start_emacs ; then
            echo "Emacs initialization failed."
            exit 1
        fi
    else
        if ! system_config ; then
            echo "Installing system configurations failed, please try again."
        fi
        for m in $@ ; do
            # Install configurations from a single package
            if [ -d $m ] ; then
                echo "Installing $m configurations."
                if ! stow $m ; then
                    echo "Stowing $m configurations failed; try again later."
                    exit 1
                else
                    shift
                fi
                if [ -f $m/install.sh ] ; then
                    echo "Running $m post-install script."
                    ./$m/install.sh
                fi
            else
                echo "No such package $m."
                exit 1
            fi
        done
        if ! start_emacs ; then
            echo "Emacs initialization failed."
            exit 1
        fi
    fi
}

# Check if root
if [ $EUID -eq 0 ] ; then
    echo "Do not run this script as root."
    echo "Doing so will clutter the root user's home directory."
    echo "You will be prompted for sudo/root password when appropriate."
    exit 1
fi

if [ -z "$1" ] ; then
    # Install for all packages
    if ! file ; then
        echo "Filesystem preparation failed."
        exit 1
    else
        if ! pkg ; then
            echo "Package installation failed."
            exit 1
        else
            if ! config ; then
                echo "Configuration installation failed."
                exit 1
            else
                echo "Done."
            fi
        fi
    fi
elif [ "$1" == "help" ] ; then
    # General help text
    usage
elif [ "$1" == "file" ] ; then
    # Call file() with all following arguments
    if ! file "${@:2}" ; then
        echo "Filesystem preparation failed."
        exit 1
    else
        echo "Done."
    fi
elif [ "$1" == "pkg" ] ; then
    # Call pkg() for all following arguments
    if ! pkg "${@:2}" ; then
        echo "Package installation failed."
        exit 1
    else
        echo "Done."
    fi
elif [ "$1" == "config" ] ; then
    # Call config() for all following arguments
    if ! config "${@:2}" ; then
        echo "Configuration installation failed."
        exit 1
    else
        echo "Done."
    fi
else
    for n in "$@" ; do
        # If arguments are package names, then do all install steps for all arguments
        if [ -d "$n" ] ; then
            if ! file "$n" ; then
                echo "${n^} filesystem preparation failed."
                exit 1
            else
                if ! pkg "$n" ; then
                    echo "${n^} package installation failed."
                    exit 1
                else
                    if ! config "$n" ; then
                        echo "${n^} configuration installation failed."
                        exit 1
                    else
                        echo "Done."
                    fi
                fi
            fi
        else
            echo "No such package $n."
            usage
            exit 1
        fi
    done
fi
