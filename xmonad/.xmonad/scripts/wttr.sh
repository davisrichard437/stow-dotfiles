#!/usr/bin/env bash
# ~/.xmonad/scripts/wttr.sh
# Weather script for xmobar

tempC=$(curl -s wttr.in\?m0TQ | awk '/°C/ {print $(NF-1)}' | sed 's/+//g')
loc=$(curl -s wttr.in\?m0Tq | awk 'FNR == 1 {print $1}')
if [ -z $tempC ] 
then
	echo ""
else
	echo "${loc%,*}: ${tempC%(*}°C | "
fi
