#!/usr/bin/env bash
# ~/.xmonad/scripts/variable.sh
# variable scripts for xmobar

upd() {
    upd=$(checkupdates | wc -l)
    if [ $upd -eq 0 ] ; then
            echo ""
    elif [ $upd -eq 1 ] ; then
            echo "$upd Update! | "
        : '
        if [ -z "$notified" ]  ; then
            action=$(dunstify --action="default,Pamac" "$upd update available")
            notified="true"
            if [ "$action" == "default" ] ; then
                pamac-manager &
                notified=""
            fi
        fi
        '
    else
        echo "$upd Updates! | "
        : '
        if [ -z "$notified" ]  ; then
            action=$(dunstify --action="default,Pamac" "$upd updates available")
            notified="true"
            if [ "$action" == "default" ] ; then
                pamac-manager &
                notified=""
            fi
        fi
        '
    fi
}

bat() {
    bat=$(acpi 2> /dev/null | sed '/unavailable/d' | cut -d ',' -f1-2 | awk '{print $NF}')
    #bat=$(acpi | cut -d ',' -f1-2 | awk '{for(i=3;i<=NF;i++) printf $i" "; print ""}')
    if [ -z "$bat" ] ; then
            echo ""
    else
            echo "Battery: $bat | "
    fi
}

echo "$(upd)$(bat)"
