#!/usr/bin/env bash
# ~/.xmonad/scripts/ping.sh
# network script for xmobar

png() {
    png=$(ping -c 1 google.com | awk 'FNR == 2 {print $(NF-1)}' | cut -d '=' -f2)
    if [ -z "$png" ] ; then
	echo "Not connected | "
    else
	echo "Ping: ${png%.*} ms | "
    fi
}
 png
