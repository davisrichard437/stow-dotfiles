#!/usr/bin/env bash
# ~/.xmonad/scripts/volume.sh
# volume script for xmobar

vol() {
	vol=$(amixer sget Master | awk 'FNR == 6 {print $(NF-1)}' | sed -e 's/\[//' -e 's/\]//')
	if [ -z "$vol" ] ; then
		echo "Volume: N/A | "
	else
		echo -e "Volume: $vol | "
	fi
}

vol
