#!/usr/bin/env bash
# ~/.xmonad/scripts/keys.sh
# script to list keybindings

sed -n '/STARTKEYS/,/ENDKEYS/p' ~/.xmonad/xmonad.hs | \
    sed -e 's/^[ \t]*-- KBGROUP /\n/g' \
        -e '/^[ \t]*--/d' \
	-e '/^$/d' \
	-e '/\]/d' \
	-e '/++/d' \
	-e '/=/d' \
	-e 's/^ *, (//g' \
	-e 's/^ *\[ (//g' \
	-e 's/\? ((//' \
	-e 's/0, //g' \
	-e 's/modMask/super/g' \
	-e 's/xK_//g' | \
    yad --fontname=Ubuntu\ Mono\ 12 --title Keybindings --no-buttons --text-info
