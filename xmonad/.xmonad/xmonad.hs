-- ~/.xmonad/xmonad.hs
-- the main config file for the XMonad window manager

import System.IO
import System.Exit

import XMonad
import XMonad.Hooks.SetWMName
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageHelpers(doFullFloat, doCenterFloat, isFullscreen, isDialog)
import XMonad.Config.Desktop
import XMonad.Config.Azerty
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.SpawnOnce
import XMonad.Actions.SpawnOn
import XMonad.Util.EZConfig (additionalKeys, additionalMouseBindings)
import XMonad.Actions.CycleWS
import XMonad.Hooks.UrgencyHook
import qualified Codec.Binary.UTF8.String as UTF8

import XMonad.Layout.Spacing
import XMonad.Layout.Gaps
import XMonad.Layout.NoBorders
import XMonad.Layout.ResizableTile
-- import XMonad.Layout.NoBorders
import XMonad.Layout.Fullscreen (fullscreenFull)
import XMonad.Layout.Cross(simpleCross)
import XMonad.Layout.Spiral(spiral)
import XMonad.Layout.ThreeColumns
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.IndependentScreens

import XMonad.Layout.CenteredMaster(centerMaster)

import Graphics.X11.ExtraTypes.XF86
import qualified XMonad.StackSet as W
import qualified Data.Map as M
import qualified Data.ByteString as B
import Control.Monad (liftM2)
import qualified DBus as D
import qualified DBus.Client as D

import Colors.Dracula

-- Adding DT's stuff
import XMonad.Layout.LayoutModifier
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import XMonad.Layout.Renamed
import XMonad.Layout.WindowNavigation
import XMonad.Layout.SubLayouts
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Simplest
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.Accordion
import XMonad.Actions.MouseResize
import XMonad.Layout.Magnifier
import XMonad.Layout.SimplestFloat
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import Data.Monoid

myStartupHook = do
    setWMName "LG3D"
    spawnOnce "xsetroot -cursor_name left_ptr"
    spawnOnce "/usr/lib/geoclue-2.0/demos/agent"
    spawnOnce "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
    -- spawnOnce "light-locker --lock-on-suspend --lock-on-lid --lock-after-screensaver 60"
    spawnOnce "dunst -config ~/.config/dunst/dunstrc"
    spawnOnce "emacs --daemon"
    -- spawnOnce "picom --experimental-backends --config ~/.config/picom/picom.conf"
    spawnOnce "redshift"
    spawnOnce "ibus-daemon -drxR"
    spawnOnce "parcellite"
    spawnOnce "~/.config/powerkit/power-management.sh"
    spawnOnce "~/.fehbg"

myNormColor :: String       -- Border color of normal windows
myNormColor = colorBack     -- This variable is imported from Colors.Dracula

myFocusColor :: String      -- Border color of focused windows
myFocusColor = colorFocus   -- This variable is imported from Colors.Dracula

myBorderWidth :: Dimension
myBorderWidth = 2           -- Sets border width for windows

--mod4Mask= super key
--mod1Mask= alt key
--controlMask= ctrl key
--shiftMask= shift key

myModMask = mod4Mask
encodeCChar = map fromIntegral . B.unpack
myFocusFollowsMouse = True
-- myBorderWidth = 2
xmobarEscape :: String -> String
xmobarEscape = concatMap doubleLts
    where
        doubleLts '<' = "<<"
        doubleLts x   = [x]

myWorkspaces = clickable . map xmobarEscape
               $ ["WWW", "MUS", "DEV", "MTG", "DOC", "MED", "SYS", "MSC"] -- desktop
  where
        clickable l = [ "<action=xdotool key super+" ++ show n ++ ">" ++ ws ++ "</action>" |
                      (i,ws) <- zip [1..9] l,
                      let n = i ]

myBaseConfig = desktopConfig

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     -- 'doFloat' forces a window to float.  Useful for dialog boxes and such.
     -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8!
     -- I'm doing it this way because otherwise I would have to write out the full
     -- name of my workspaces and the names would be very long if using clickable workspaces.
     [ className =? "confirm"         --> doFloat
     , className =? "file_progress"   --> doFloat
     , className =? "dialog"          --> doFloat
     , className =? "download"        --> doFloat
     , className =? "error"           --> doFloat
     -- , className =? "Gimp"            --> doFloat
     , className =? "notification"    --> doFloat
     , className =? "pinentry-gtk-2"  --> doFloat
     , className =? "splash"          --> doFloat
     , className =? "toolbar"         --> doFloat
     , className =? "Yad"             --> doCenterFloat
     , className =? "Gmrun"             --> doCenterFloat
     , title =? "Oracle VM VirtualBox Manager"  --> doFloat
     , title =? "Mozilla Firefox"     --> doShift ( myWorkspaces !! 0 )
     , className =? "qutebrowser"     --> doShift ( myWorkspaces !! 0 )
     , className =? "Brave-browser"   --> doShift ( myWorkspaces !! 0 )
     , className =? "MuseScore3"      --> doShift ( myWorkspaces !! 1 )
     , title =? "Virtual MIDI Piano Keyboard"      --> doShift ( myWorkspaces !! 1 )
     , title =? "REAPER"      --> doShift ( myWorkspaces !! 1 )
     , title =? "REAPER (initializing)"      --> doShift ( myWorkspaces !! 1 )
     , className =? "Audacity"      --> doShift ( myWorkspaces !! 1 )
     , title =? "Audacity is starting up..."      --> doShift ( myWorkspaces !! 1 )
     , className =? "discord"             --> doShift ( myWorkspaces !! 3 )
     , className =? "zoom"             --> doShift ( myWorkspaces !! 3 )
     , className =? "Signal"             --> doShift ( myWorkspaces !! 3 )
     , title =? "LibreOffice"             --> doShift ( myWorkspaces !! 4 )
     , className =? "kdenlive"             --> doShift ( myWorkspaces !! 5 )
     , className =? "mpv"             --> doShift ( myWorkspaces !! 5 )
     , className =? "vlc"             --> doShift ( myWorkspaces !! 5 )
     , className =? "Gimp"            --> doShift ( myWorkspaces !! 5 )
     , className =? "Pavucontrol"            --> doShift ( myWorkspaces !! 6 )
     , className =? "Pamac-manager"            --> doShift ( myWorkspaces !! 6 )
     , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
     , isFullscreen -->  doFullFloat
     ]

-- window manipulations
-- myManageHook = composeAll . concat $
--     [ [isDialog --> doCenterFloat]
--     , [className =? c --> doCenterFloat | c <- myCFloats]
--     , [title =? t --> doFloat | t <- myTFloats]
--     , [resource =? r --> doFloat | r <- myRFloats]
--     , [resource =? i --> doIgnore | i <- myIgnores]
--     , [(className =? x <||> title =? x <||> resource =? x) --> doShift "WWW" | x <- my1Shifts]
--     , [(className =? x <||> title =? x <||> resource =? x) --> doShift "MUS" | x <- my2Shifts]
--     , [(className =? x <||> title =? x <||> resource =? x) --> doShift "TTY" | x <- my3Shifts]
--     , [(className =? x <||> title =? x <||> resource =? x) --> doShift "MTG" | x <- my4Shifts]
--     , [(className =? x <||> title =? x <||> resource =? x) --> doShift "DOC" | x <- my5Shifts]
--     , [(className =? x <||> title =? x <||> resource =? x) --> doShift "MED" | x <- my6Shifts]
--     , [(className =? x <||> title =? x <||> resource =? x) --> doShift "SYS" | x <- my7Shifts]
--     , [(className =? x <||> title =? x <||> resource =? x) --> doShift "MSC" | x <- my8Shifts]
--     , [isFullscreen --> doFullFloat]
--     ]
--     where
--     -- doShiftAndGo = doF . liftM2 (.) W.greedyView W.shift
--     myCFloats = ["Arandr", "Galculator", "feh", "mpv", "Xfce4-terminal"]
--     myTFloats = ["Downloads", "Save As..."]
--     myRFloats = []
--     myIgnores = ["desktop_window"]
--     my1Shifts = ["Firefox"]
--     my2Shifts = ["MuseScore", "Virtual MIDI Piano Keyboard", "REAPER", "REAPER (initializing)", "Audacity", "Audacity is starting up..."]
--     my3Shifts = []
--     my4Shifts = ["Zoom", "discord"]
--     my5Shifts = ["LibreOffice", "ghostwriter"]
--     my6Shifts = ["vlc", "mpv"]
--     my7Shifts = ["Pavucontrol", "Pamac-manager"]
--     my8Shifts = []

--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- Defining a bunch of layouts, many that I don't use.
-- limitWindows n sets maximum number of windows displayed for layout.
-- mySpacing n sets the gap size around the windows.
tall     = renamed [Replace "tall"]
           $ smartBorders
           $ windowNavigation
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 6
           $ ResizableTall 1 (3/100) (1/2) []
wide     = renamed [Replace "wide"]
           $ smartBorders
           $ windowNavigation
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 6
           $ Mirror
           $ ResizableTall 1 (3/100) (1/2) []
monocle  = renamed [Replace "monocle"]
           $ smartBorders
           $ windowNavigation
           -- $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 20 Full
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat

-- The layout hook
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     withBorder myBorderWidth tall
                                 ||| withBorder myBorderWidth wide
                                 ||| noBorders monocle
                                 ||| floats

-- myLayout = spacingRaw True (Border 0 6 0 6) True (Border 6 0 6 0) True $ avoidStruts $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) $ tiled ||| Mirror tiled ||| Full
-- myLayout = spacingRaw False (Border 6 6 6 6) True (Border 6 6 6 6) True $ avoidStruts $ mkToggle (MIRROR ?? NBFULL ?? NOBORDERS ?? EOT) $ tiled ||| Mirror tiled ||| Full
--     where
--         tiled = Tall 1 (3/100) (1/2)

myMouseBindings (XConfig {XMonad.modMask = modMask}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modMask, 1), (\w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modMask, 2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modMask, 3), (\w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster))

    -- mod-scroll, move workspaces
    , ((modMask, 5), (\w -> nextWS))
    , ((modMask, 4), (\w -> prevWS))
    ]


-- STARTKEYS

myKeys conf@(XConfig {XMonad.modMask = modMask}) = M.fromList $
  -- KBGROUP Spawn:

  [ ((modMask, xK_e), spawn $ "emacsclient -c -a emacs" )
  , ((modMask .|. shiftMask, xK_e), spawn $ "gvim" )
  , ((modMask .|. controlMask, xK_e), spawn $ "~/.local/bin/rofi-edit" )
  --, ((modMask .|. controlMask, xK_f), sendMessage $ Toggle MIRROR)
  -- Super + h/l go to prev/next workspace
  , ((modMask, xK_h), prevWS)
  , ((modMask, xK_l), nextWS)

  , ((modMask, xK_w), kill )
  , ((modMask, xK_r), spawn $ "rofi -show drun" )
  , ((modMask .|. shiftMask, xK_w), spawn $ "rofi -show window -window-format '{c}  {t}'" )
  , ((modMask, xK_s), spawn $ "~/.local/bin/rofi-power-menu" )
  , ((modMask .|. shiftMask, xK_s), spawn $ "~/.xmonad/scripts/keys.sh" )
  , ((modMask .|. shiftMask, xK_r), spawn $ "gmrun" )
  , ((modMask, xK_v), spawn $ "vmpk" )
  , ((modMask .|. shiftMask, xK_v), spawn $ "vlc" )
  , ((modMask, xK_m), spawn $ "env PULSE_LATENCY_MSEC=30 mscore" )
  , ((modMask, xK_d), spawn $ "reaper" )
  , ((modMask .|. shiftMask, xK_d), spawn $ "audacity" )
  , ((modMask, xK_p), spawn $ "pamac-manager" )
  , ((modMask .|. shiftMask, xK_p), spawn $ "pavucontrol" )
  , ((modMask, xK_f), spawn $ "pcmanfm" )
  , ((modMask, xK_b), spawn $ "firefox" )
  , ((modMask, xK_c), spawn $ "~/.config/picom/picom-toggle.sh" )
  , ((modMask .|. shiftMask, xK_c), spawn $ "~/.local/bin/color-scheme" )
  , ((modMask .|. shiftMask, xK_b), spawn $ "firefox --private-window" )
  , ((modMask, xK_Return), spawn $ "alacritty" )
  , ((modMask .|. shiftMask, xK_Return), spawn $ "pcmanfm" )
  , ((0, xK_Print), spawn $ "gnome-screenshot --interactive")

  -- KBGROUP XMonad:
  , ((modMask, xK_q ), spawn $ "xmonad --recompile && xmonad --restart")
  , ((modMask .|. shiftMask , xK_q ), io (exitWith ExitSuccess))




  -- KBGROUP Multimedia:

  -- Increase brightness
  , ((0, xF86XK_MonBrightnessUp), spawn $ "~/.local/bin/brightness 5%+")

  -- Decrease brightness
  , ((0, xF86XK_MonBrightnessDown), spawn $ "~/.local/bin/brightness 5%-")

  -- Increase volume
  , ((0, xF86XK_AudioRaiseVolume), spawn $ "~/.local/bin/volume 5%+")

  -- Decrease volume
  , ((0, xF86XK_AudioLowerVolume), spawn $ "~/.local/bin/volume 5%-")

  -- Mute
  , ((0, xF86XK_AudioMute), spawn $ "~/.local/bin/volume 0%")

--  , ((0, xF86XK_AudioPlay), spawn $ "mpc toggle")
--  , ((0, xF86XK_AudioNext), spawn $ "mpc next")
--  , ((0, xF86XK_AudioPrev), spawn $ "mpc prev")
--  , ((0, xF86XK_AudioStop), spawn $ "mpc stop")

  -- Check these out on desktop sometime maybe?
  --, ((0, xF86XK_AudioPlay), spawn $ "playerctl play-pause")
  --, ((0, xF86XK_AudioNext), spawn $ "playerctl next")
  --, ((0, xF86XK_AudioPrev), spawn $ "playerctl previous")
  --, ((0, xF86XK_AudioStop), spawn $ "playerctl stop")


  --------------------------------------------------------------------
  -- KBGROUP Layout:

  -- Cycle through the available layout algorithms.
  , ((modMask, xK_Tab), sendMessage NextLayout)

  --  Reset the layouts on the current workspace to default.
  , ((modMask .|. shiftMask, xK_Tab), setLayout $ XMonad.layoutHook conf)

  -- Move focus to the next window.
  , ((modMask, xK_j), windows W.focusDown)

  -- Move focus to the previous window.
  , ((modMask, xK_k), windows W.focusUp  )

  -- Move focus to the master window.
  , ((modMask .|. shiftMask, xK_m), windows W.focusMaster  )

  -- Swap the focused window with the next window.
  , ((modMask .|. shiftMask, xK_j), windows W.swapDown  )

  -- Swap the focused window with the previous window.
  , ((modMask .|. shiftMask, xK_k), windows W.swapUp    )

  -- Shrink the master area.
  , ((modMask .|. shiftMask , xK_h), sendMessage Shrink)

  -- Expand the master area.
  , ((modMask .|. shiftMask , xK_l), sendMessage Expand)

  -- Push window back into tiling.
  , ((modMask, xK_t), withFocused $ windows . W.sink)

  -- Fullscreen
  -- , ((modMask .|. shiftMask, xK_f), sendMessage (Toggle NBFULL) >> sendMessage ToggleStruts)

  -- Increment/deincrement the number of windows in the master area.
  , ((modMask, xK_comma), sendMessage (IncMasterN 1))
  , ((modMask, xK_period), sendMessage (IncMasterN (-1)))

  ]
  ++
  -- ENDKEYS
  -- mod-[1..8], Switch to workspace N
  -- mod-shift-[1..8], Move client to workspace N
  [((m .|. modMask, k), windows $ f i)

  --Keyboard layouts
  --qwerty users use this line
   | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_8] -- laptop
      , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
      --, (\i -> W.greedyView i . W.shift i, shiftMask)]]

  -- ++
  -- ctrl-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
  -- ctrl-shift-{w,e,r}, Move client to screen 1, 2, or 3
  --[((m .|. controlMask, key), screenWorkspace sc >>= flip whenJust (windows . f))
      -- | (key, sc) <- zip [xK_w, xK_e] [0..]
      -- , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


main :: IO ()
main = do
    xmproc <- spawnPipe "xmobar ~/.config/xmobar/dracula-xmobarrc"
    dbus <- D.connectSession
    -- Request access to the DBus name
    D.requestName dbus (D.busName_ "org.xmonad.Log")
        [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]

    xmonad . ewmh $
            myBaseConfig

                {startupHook = myStartupHook
--, layoutHook = gaps [(U,30), (D,6), (R,6), (L,6)] $ myLayout
, layoutHook = myLayoutHook
, manageHook = manageSpawn <+> myManageHook <+> manageHook myBaseConfig
, modMask = myModMask
, borderWidth = myBorderWidth
, handleEventHook    = handleEventHook myBaseConfig <+> fullscreenEventHook
, focusFollowsMouse = myFocusFollowsMouse
, workspaces = myWorkspaces
, focusedBorderColor = myFocusColor
, normalBorderColor = myNormColor
, keys = myKeys
, mouseBindings = myMouseBindings
, logHook = dynamicLogWithPP xmobarPP
    { ppOutput = \x -> hPutStrLn xmproc x
    , ppCurrent = xmobarColor colorFocus "" . wrap "[" "]"
    , ppTitle = xmobarColor colorFore "" . shorten 50
    , ppHiddenNoWindows = xmobarColor colorFore "" . shorten 50
    , ppVisible = xmobarColor colorFore "" . shorten 50
    , ppUrgent = xmobarColor colorUrgent "" . wrap "!" "!"
    , ppSep = "<fc=" ++ colorFore ++"> | </fc>"
    }
}
