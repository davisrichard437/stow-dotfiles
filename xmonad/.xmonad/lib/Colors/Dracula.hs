module Colors.Dracula where

import XMonad

colorBack = "#282a36"
colorFore = "#f8f8f2"
colorFocus = "#bd93f9"
colorUrgent = "#ff5555"
