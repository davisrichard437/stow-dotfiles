# XMonad

These are the files required for the XMonad configuration.

## Dependencies

- alacritty
- amixer
- audacity
- brightnessctl
- curl
- dunst
- feh
- firefox
- geoclue-2.0
- gmrun
- gnome-screenshot
- ibus-daemon
- light-locker
- musescore
- noto-fonts-cjk
- pacman-contrib
- parcellite
- pavucontrol
- pcmanfm
- picom-ibhagwan-git
- polkit-gnome
- reaper
- rofi
- gvim
- vlc
- vmpk
- xdotool
- xmobar
- xmonad
- xmonad-contrib
- xmonad-utils

## Files

- `~/.xmonad/xmonad.hs`: the main config file for the XMonad window manager.
- `~/.xmobarrc`: the config file for the XMobar status bar.
- `~/.stalonetrayrc`: the config file for the stalonetray systray.
- `~/.xmonad/scripts/keys.sh`: script to list keybindings.
- `~/.xmonad/scripts/menu.sh`: script to provide simple dropdown application menu.
- `~/.xmonad/scripts/*`: scripts to populate xmobar.

## Screenshot

![xmonad-dracula](xmonad/screenshot.png)
