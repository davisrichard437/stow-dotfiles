" ~/.gvimrc
" config file for the gvim text editor

colorscheme dracula
set bg=dark
set guifont=Hack\ Nerd\ Font\ Mono\ 12
set guioptions-=T
set guioptions-=m
set guioptions-=r
