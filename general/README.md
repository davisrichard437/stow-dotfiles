# General

## Introduction

These files are those shared by the other packages. For example, the other window manager packages all use rofi as a run launcher, so its configs are stored here.

## Dependencies

- alacritty
- dunst
- emacs
- gtk
	- GTK Theme: [dracula](https://draculatheme.com/gtk)
	- Icon Theme: [tela-purple-dark](https://www.gnome-look.org/s/Gnome/p/1279924)
- picom-ibhagwan-git
- redshift
- rofi
- ttf-ubuntu-font-family
- gvim
- xclickroot
- xmenu
- zsh
	- zsh-syntax-highlighting-git

## Files

- `~/.config/alacritty/alacritty.yml`: config file for the alacritty terminal emulator.
- `~/.config/dunst/dunstrc`: config file for the dunst notification daemon.
- `~/.config/fontconfig/fonts.conf`: system font config file.
- `~/.config/gtk-3.0/settings.ini`: gtk-3.0 theme setting file.
- `~/.config/picom/picom.conf`: config file for the picom-ibhagwan-git fork of the picom compositor.
- `~/.config/picom/picom-toggle.sh`: script to restart picom.
- `~/.config/rofi/themes/dracula.rasi`: dracula theme for rofi.
- `~/.config/rofi/config.rasi`: rofi theme selection file.
- `~/.config/redshift.conf`: config file for redshift color correction.
- `~/.emacs.d/init.el`: config file for the emacs text editor.
- `~/.local/bin/*`: small collection of scripts.
- `~/Wallpapers/*`: small collection of wallpapers.
- `~/.bashrc`: config file for the bash shell.
- `~/.gmrunrc`: gmrun configuration file.
- `~/.gtkrc-2.0`: gtk-2.0 theme setting file.
- `~/.gvimrc`: config file for the gvim text editor.
- `~/.vimrc`: config file for the vim text editor.
- `~/.xprofile`: early autostart script.
- `~/.Xresources`: xresources file.
- `~/.zshrc`: config file for the zsh shell.
