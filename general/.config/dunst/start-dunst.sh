#!/usr/bin/env bash
# ~/.config/dunst/dunstrc
# script to start dunst with the correct config

if pgrep dunst ; then
    pkill -9 dunst
fi

if ! pgrep awesome ; then
    dunst -config ~/.config/dunst/dracula-dunstrc &
fi
