#!/usr/bin/env bash
# ~/.config/picom/picom-toggle.sh
# script to toggle picom

if [ $(ps -aux | grep [p]icom.conf | wc -l) -gt 0 ] ; then
    pkill -9 picom
else
    picom --experimental-backends --config ~/.config/picom/picom.conf
fi
