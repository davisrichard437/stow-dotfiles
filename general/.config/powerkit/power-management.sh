#!/usr/bin/env bash
# ~/.config/powerkit/power-management.sh
# script to start laptop power management

function run {
    if ! pgrep $1 ;
    then
        $@&
    fi
}

pm() {
    # function to run power management
    # run cbatticon
    # run xscreensaver --no-splash
    run mate-power-manager
    run light-locker --lock-on-suspend --lock-on-lid --lock-after-screensaver 60
}

cid=$(cat /sys/class/dmi/id/chassis_type)

if [ $cid -eq 8 ] ; then
    pm
elif [ $cid -eq 9 ] ; then
    pm
elif [ $cid -eq 10 ] ; then
    pm
elif [ $cid -eq 11 ] ; then
    pm
else
    run light-locker --lock-on-suspend --lock-on-lid --lock-after-screensaver 60
fi
