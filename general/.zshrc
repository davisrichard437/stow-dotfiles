# ~/.zshrc
# config file for the zsh shell

# zsh prompt enable
git_prompt() {
    ref=$(git symbolic-ref HEAD 2> /dev/null | cut -d'/' -f3-)
    if [ ! -z "$ref" ] ; then
        echo " @ $ref "
    else
        echo " "
    fi
}
setopt prompt_subst
autoload -Uz promptinit compinit
compinit
promptinit
PROMPT='%F{green}%n%f@%F{magenta}%m%f %F{blue}%B%~%b%f%F{yellow}$(git_prompt)%f%# '
#prompt pure

# Add ~/.bin and ~/.local/bin to $PATH
if [[ -d "$HOME/.bin" ]] ; then
    PATH="$HOME/.bin:$PATH"
fi

if [[ -d "$HOME/.local/bin" ]] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

export EDITOR='vim'
export VISUAL='vim'

# NNN Options
export NNN_OPTS='H'
alias "nnn"="nnn -e"

# zsh syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Better history search
export HISTFILE=~/.zsh_history
export HISTSIZE=10000
export SAVEHIST=10000
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_REDUCE_BLANKS
setopt INC_APPEND_HISTORY_TIME
autoload -U history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end

# Enable command correction
setopt CORRECT

# Keybindings
# https://zsh.sourceforge.io/Doc/Release/Zsh-Line-Editor.html#Zle-Builtins for functions
# $ showkey -a for key sequences
bindkey "^[[A" history-beginning-search-backward-end
bindkey "^[[B" history-beginning-search-forward-end
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey "^H" backward-kill-word
bindkey "^[[3;5~" delete-word
bindkey "\e[3~" delete-char
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line

# Aliases
alias ymd='date +%F'

alias ls='ls --color=auto'
alias la='ls -a'
alias ll='ls -lah'
alias l='ls'
alias l.="ls -A | egrep '^\.'"

alias df='df -h -x squashfs -x tmpfs -x devtmpfs'
alias du='du -h'

alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

alias speedtest="curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python3 -"

alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"

alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'
alias pkgcount='pacman -Q | wc -l'

alias vlightdm="sudo vim /etc/lightdm/lightdm.conf"
alias vpacman="sudo vim /etc/pacman.conf"
alias vgrub="sudo vim /etc/default/grub"
alias vconfgrub="sudo vim /boot/grub/grub.cfg"
alias vmkinitcpio="sudo vim /etc/mkinitcpio.conf"
alias vmirrorlist="sudo vim /etc/pacman.d/mirrorlist"
alias vfstab="sudo vim /etc/fstab"
alias vb="vim ~/.bashrc"
alias vz="vim ~/.zshrc"

alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "
alias ytv-best="youtube-dl -f bestvideo+bestaudio "

alias pandoc="pandoc -V margin-top=1in -V margin-left=1in -V margin-right=1in -V margin-bottom=1in -V papersize=letter"

alias ix="curl -F 'f:1=<-' ix.io"

alias git-update="git commit -am 'Minor updates.' && git push"
alias update="paru && sync-dotfiles"
alias wttr="curl wttr.in\?m"
alias sbcl="rlwrap sbcl"

alias sn="shutdown now"
alias sr="reboot"

ex () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xvjf $1   ;;
      *.tar.gz)    tar xvzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xvf $1    ;;
      *.tbz2)      tar xvjf $1   ;;
      *.tgz)       tar xvzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xvf $1    ;;
      *.tar.zst)   tar xvf $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
