;; Increasing garbage collection threshold for faster startup execution
(setq gc-cons-threshold (* 50 1000 1000))

(defun display-startup-time ()
  "Function to display startup time"
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
                     (time-subtract after-init-time before-init-time)))
           gcs-done))

(add-hook 'emacs-startup-hook #'display-startup-time)

;; Set up package.el to work with MELPA, ORG, and ELPA
(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("melpa" . "https://melpa.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Install use-package
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
;; Always install package if not already present.
(setq use-package-always-ensure t)

(use-package auto-package-update
  :custom
  (auto-package-update-interval 7)               ; update every 7 days
  (auto-package-update-prompt-before-update nil) ; do not prompt before updating
  :config
  (auto-package-update-maybe)
  (auto-package-update-at-time "09:00"))

(setq inhibit-startup-message t)      ; remove default emacs startup message
(menu-bar-mode -1)                    ; remove menubar (file, edit, etc)
(tool-bar-mode -1)                    ; remove toolbar (save, quit, etc)
(scroll-bar-mode -1)                  ; remove scrollbar
(set-fringe-mode 10)                  ; left/right pixel margin

;; scroll one line at a time (less "jumpy" than defaults)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))   ; one line at a time
      mouse-wheel-progressive-speed nil              ; don't accelerate scrolling
      mouse-wheel-follow-mouse 't                    ; scroll window under mouse
      scroll-step 1)                                 ; keyboard scroll one line at a time

(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

(defun set-font-faces ()
  "Sets default, fixed- and variable-pitch font faces."
  (message "Setting font faces.")
  (add-to-list 'default-frame-alist '(font . "Hack Nerd Font Mono 12"))
  (set-face-attribute 'default nil :family "Hack Nerd Font Mono" :height 120)
  (set-face-attribute 'fixed-pitch nil :font "Hack Nerd Font Mono" :height 120 :weight 'regular)
  (set-face-attribute 'variable-pitch nil :font "Ubuntu" :height 140 :weight 'regular)

  ;; Emojis
  (set-fontset-font
   t
   '(#x1f300 . #x1fad0)
   (cond
    ((member "Noto Color Emoji" (font-family-list)) "Noto Color Emoji")
    ((member "Symbola" (font-family-list)) "Symbola")))

  ;; JP
  (set-fontset-font
   t
   'han
   (cond
    ((member "Noto Sans CJK JP" (font-family-list)) "Noto Sans CJK JP")
    ((member "Noto Sans CJK TC" (font-family-list)) "Noto Sans CJK TC"))))

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (with-selected-frame frame
                  (set-font-faces))))
  (set-font-faces))

(require 'display-line-numbers)

(defcustom display-line-numbers-exempt-modes '(vterm-mode
                                               eshell-mode
                                               shell-mode
                                               term-mode
                                               ansi-term-mode
                                               org-mode
                                               treemacs-mode
                                               pdf-view-mode
                                               dashboard-mode)
  "Major modes on which to disable line numbers."
  :group 'display-line-numbers
  :type 'list
  :version "green")

(defun display-line-numbers--turn-on ()
  "Turn on line numbers except for certain major modes.
Exempt major modes are defined in `display-line-numbers-exempt-modes'."
  (unless (or (minibufferp)
              (member major-mode display-line-numbers-exempt-modes))
    (display-line-numbers-mode)))

(column-number-mode)
(global-display-line-numbers-mode)        ; Display column numbers in modeline

;; Modeline configuration
(use-package doom-modeline
  :init
  (doom-modeline-mode 1)
  (unless (file-exists-p "~/.local/share/fonts/all-the-icons.ttf")
    (all-the-icons-install-fonts t))
  :custom (doom-modeline-height 25))

;; Theme setting
(use-package doom-themes
  :init (load-theme 'doom-dracula t)
  :config
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t)
  (doom-themes-org-config))

(defun untabify-all ()
  "Untabify entire file, unless `inhibit-untabify' is non-nil."
  (interactive)
  (unless inhibit-untabify
    (untabify (point-min) (point-max))))

(defun force-untabify-all ()
  "Force untabify files that have some read-only text."
  (interactive)
  (let ((inhibit-read-only t)) (untabify-all)))

(defvar inhibit-untabify nil
  "Buffer-local variable checked by `untabify-all'")

(setq-default tab-width 4)
(setq indent-tabs-mode nil)
(setq tab-stop-list (number-sequence tab-width 120 tab-width))
(setq evil-shift-width tab-width)          ; shift width for evil < and > keys
(add-hook 'before-save-hook 'untabify-all) ; convert tabs to spaces on save

(recentf-mode 1)                      ; save recent files across sessions
(setq recentf-max-menu-items 20       ; maximum displayed recent files
      recentf-max-saved-items 20)     ; maximum recent files saved across sessions

;; Create autosave directory unless it exists.
;; Autosaves don't automatically create parent directory.
(unless (file-directory-p (concat user-emacs-directory "autosaves"))
  (make-directory (concat user-emacs-directory "autosaves")))

(setq
  vc-make-backup-files t ; back up versioned files
  backup-by-copying t    ; don't clobber symlinks
  backup-directory-alist ; keep all backups in central folder
     `(("." . ,(concat user-emacs-directory "backups")))
  auto-save-file-name-transforms
     `((".*" "~/.emacs.d/autosaves/" t)) ; keep all autosaves in central folder
  delete-old-versions t  ; delete old backups
  kept-new-versions 6    ; number of new versions to keep
  kept-old-versions 2    ; number of old versions to keep
  version-control t)     ; version control backup names

;; Save custom variables externally
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(if (file-exists-p custom-file)
    (load-file custom-file))

(use-package dashboard
  :init
  (setq dashboard-startup-banner 'logo    ; display unofficial emacs logo
        dashboard-items '((recents . 10)  ; define the contents of dashboard
                          (projects . 5)
                          (bookmarks . 5)
                          (agenda . 5)))
  :config
  (dashboard-setup-startup-hook)
  ;; set initial buffer to dashboard
  (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*"))))

(use-package evil
  :init
  (setq evil-want-keybinding nil)
  (setq evil-undo-system 'undo-fu)        ; enable C-r redo functionality
  (setq evil-want-Y-yank-to-eol t)        ; same as nmap Y y$
  (setq evil-want-C-u-delete t)           ; C-u delete to indentation
  (setq evil-want-C-w-delete t)           ; C-w delete word backward
  (setq evil-want-C-i-jump nil)           ; don't overwrite C-i (inserts tab)
  :config
  (evil-mode 1)                                                       ; enable evil
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)   ; C-g = ESC
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)            ; j go down visually
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)        ; k go up visually
  ;; (evil-global-set-key 'motion "/" 'swiper)                        ; bind / to swiper
  (evil-global-set-key 'motion "SPC" nil))                            ; unbind prefix key

(use-package evil-collection
  :after evil
  :init
  :config
  (dolist (mode '(tetris))
    (setq evil-collection-mode-list (delq mode evil-collection-mode-list)))
  (evil-collection-init))

(use-package evil-surround
  :ensure t
  :config
  (global-evil-surround-mode 1))

(use-package vimish-fold
  :hook vimish-fold-mode-hook
  :config
  (require 'vimish-fold)
  (vimish-fold-global-mode 1)
  (define-key evil-normal-state-map (kbd "TAB") 'vimish-fold-toggle))

(use-package company
  :hook ((prog-mode
          inferior-python-mode
          inferior-lisp-mode) . company-mode)
  :bind (:map company-active-map
         ("<tab>" . company-complete-selection))
        (:map lsp-mode-map
         ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode))

(electric-pair-mode)                  ; automatically insert matching delimiter
(show-paren-mode 1)                   ; highlight matching delimiter

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :bind                     ; remap emacs documentation functions to helpful
  ([remap describe-command] . helpful-command)
  ([remap describe-function] . helpful-function)
  ([remap describe-key] . helpful-key)
  ([remap describe-macro] . helpful-macro)
  ([remap describe-mode] . helpful-mode)
  ([remap describe-symbol] . helpful-symbol)
  ([remap describe-variable] . helpful-variable))

(defun minibuffer-backward-kill (arg)
  "When minibuffer is completing a file name delete up to parent
folder, otherwise delete a word"
  (interactive "p")
  (if minibuffer-completing-file-name
      ;; Borrowed from https://github.com/raxod502/selectrum/issues/498#issuecomment-803283608
      (if (string-match-p "/." (minibuffer-contents))
          (zap-up-to-char (- arg) ?/)
        (delete-minibuffer-contents))
      (delete-word (- arg))))

(use-package vertico
  :bind (:map vertico-map
         ("C-j" . vertico-next)
         ("C-k" . vertico-previous)
         ("C-f" . vertico-exit)
         ("C-<backspace>" . minibuffer-backward-kill)
         ("M-RET" . #'minibuffer-force-complete-and-exit))
  :custom
  (vertico-cycle t)
  :init
  (vertico-mode))

(use-package savehist
  :ensure nil
  :init
  (savehist-mode))

(use-package marginalia
  :after vertico
  :custom
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  :init
  (marginalia-mode))

(use-package consult
  :bind (("C-s" . consult-line)
         :map minibuffer-local-map
         ("C-r" . consult-history))
  :config
  (setq completion-in-region-function
        'consult-completion-in-region))

(use-package orderless
  :ensure t
  :custom (completion-styles '(orderless)))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode)

(use-package rainbow-mode
  :hook prog-mode)

(defun check-music (str)
  "Checks output of `dired-show-file-type' if file is music."
  (interactive)
  (if (eq (eval-expression (count-windows)) 1)
      (delete-frame)
    (delete-window)))

(defun dired-default ()
  "Dired jump to current buffer directory or to default if none."
  (interactive)
  (if buffer-file-name
      (dired (file-name-directory buffer-file-name))
    (dired default-directory)))

(defun dired-open-file ()
  "Open file appropriately in dired."
  (interactive)
  (cond
   ;; Open directories in dired
   ((file-directory-p (dired-get-file-for-visit))
    (dired-single-buffer))
   ;; Play music in emms
   ((check-music (dired-show-file-type (dired-get-file-for-visit)))
    (emms-play-file (dired-get-file-for-visit)))
   ;; All other files in emacs
   (t
    (dired-single-buffer))))

(defun my-file-find-hook ()
  "Enables checking for non-editable files and calls sudo-edit accordingly."
  (when (and (not (file-writable-p buffer-file-name))
             (y-or-n-p-with-timeout "File not writable. Open as root?" 5 nil))
    (let ((obuf (current-buffer)))
      (sudo-edit)
      (unless (equal (current-buffer) obuf)
        (let (kill-buffer-query-functions kill-buffer-hook)
          (kill-buffer obuf))))))

(add-hook 'find-file-hook 'my-file-find-hook)
(require 'evil-collection)
(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :custom (dired-listing-switches "-agho --group-directories-first") ; specifies ls switches
  :config
  (define-key dired-mode-map (kbd " ") nil) ; undefine prefix key
  (evil-collection-define-key 'normal 'dired-mode-map
    "h" 'dired-single-up-directory    ; h goes up a directory
    "l" 'dired-open-file              ; l opens the file under the point
    "f" 'find-file                    ; f jumps to file (or creates it if nonexistent)
    " " nil)                          ; undefine prefix key
  (setq dired-auto-revert-buffer t))  ; automatically redisplay buffer when revisiting

(use-package dired-single
  :after dired
  :commands (dired dired-jump))

(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode)) ; enable filetype icons in dired buffers

(defun configure-eshell ()
  "Eshell configuration."
  ;; Save command history when commands are entered
  (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)
  ;; Truncate buffer for performance
  (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer)
  ;; Bind some useful keys for evil-mode
  (evil-define-key '(normal insert visual) eshell-mode-map (kbd "C-r") 'consult-history)
  (evil-define-key '(normal insert visual) eshell-mode-map (kbd "<home>") 'eshell-bol)
  (define-key eshell-mode-map (kbd "TAB") 'completion-at-point) ; Vertico integration
  (evil-normalize-keymaps)
  (setq eshell-history-size         10000    ; maximum saved history
        eshell-buffer-maximum-lines 10000    ; maximum buffer scrollback
        eshell-hist-ignoredups t))           ; ignore duplicates

(use-package eshell-git-prompt
  :after eshell)

(use-package eshell
  :hook (eshell-first-time-mode . configure-eshell)
  :config
  (with-eval-after-load 'esh-opt
    (setq eshell-destroy-buffer-when-process-dies t)      ; kill buffer on exit
    (setq eshell-visual-commands '("htop" "zsh" "vim")))  ; enable htop, zsh, and vim
  (eshell-git-prompt-use-theme 'powerline))

(use-package term
  :ensure nil
  :commands term
  :config
  (setq terminal-prompt-regexp "[~\/A-Za-z-.@ ]*[%\$]"))

(use-package vterm
  :commands vterm
  :init
  (setq vterm-always-compile-module t)    ; always compile module without prompt
  :config
  (setq vterm-kill-buffer-on-exit t))     ; kill buffer on exit

(defun lsp-mode-setup ()
  (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
  (lsp-headerline-breadcrumb-mode))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :hook (lsp-mode . lsp-mode-setup)
  :custom (lsp-keymap-prefix "C-c l")
  :init
  (setq lsp-keymap-prefix "C-c l")        ; map lsp to "C-c l"
  :config
  (lsp-enable-which-key-integration t))   ; which-key hinting

;; Indentation settings
(put 'lambda 'lisp-indent-function 'defun)
(put 'while 'lisp-indent-function 1)
(put 'if 'lisp-indent-function 2)
(put 'when 'lisp-indent-function 1)
(put 'unless 'lisp-indent-function 1)
(put 'do 'lisp-indent-function 2)
(put 'do* 'lisp-indent-function 2)

(defun initialize-slime ()
  "Function to initialize slime, the superior lisp implementation for emacs."
  (setq inferior-lisp-program "/usr/bin/sbcl") ; use sbcl for lisp executable
  (add-to-list 'load-path "/usr/share/emacs/site-lisp/slime/") ; use slime repl
  (require 'slime)
  (slime-setup))

(if (executable-find "sbcl")
    (initialize-slime))

;; Bash language server
(add-hook 'sh-mode-hook 'lsp-deferred)
;; (add-hook 'sh-mode-hook 'company-mode)

(autoload 'LilyPond-mode "lilypond-mode")
(setq auto-mode-alist                 ; automatically load lilypond-mode on *.ly files
      (cons '("\\.ly\\'" . LilyPond-mode) auto-mode-alist))
(add-hook 'LilyPond-mode-hook (lambda () (turn-on-font-lock)))

(use-package python-mode
  :mode "\\.py\\'"
  :hook (python-mode . lsp-deferred)
  :custom
  (python-shell-interpreter "python3"))

(use-package lsp-pyright
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp-deferred))))

(use-package magit
  :diminish auto-revert
  :commands magit-status
  :custom
  ;; display in same buffer
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(setq vc-follow-symlinks t) ; follow version-controlled symlinks without prompt

(setq my-projectile-search-path '())

(defun add-projectile-search-path (file)
  "Function to add file to org agenda if it exists."
  (if (file-directory-p file)
      (setq my-projectile-search-path
            (add-to-list 'my-projectile-search-path file))))

(mapcar 'add-projectile-search-path
        '("~/Documents/Projects/Code/"
          "~/Documents/"
          "~/Projects/"
          "~/"))

(use-package projectile
  :commands projectile-command-map
  :diminish projectile-mode
  :custom ((projectile-completion-system 'auto)
           (projectile-project-search-path my-projectile-search-path))
  :init
  (setq projectile-switch-project-action #'projectile-dired)
  :config (projectile-mode)
  :bind-keymap
  ("C-c p" . projectile-command-map))           ; map to "C-c p" (use "SPC p" instead)

(defun org-babel-tangle-config ()
  "Function to tangle literate config."
  (when (and (file-exists-p (buffer-file-name))
             (string= "Emacs.org" (file-name-nondirectory (buffer-file-name))))
    (let ((org-confirm-babel-evaluate nil))
      (org-babel-tangle))))

(defun org-mode-setup ()
  "Setup for org mode."
  (org-indent-mode)
  (variable-pitch-mode 1)
  ;(auto-fill-mode 0)
  (visual-line-mode 1))

(defun org-mode-visual-fill ()
  "Format text in org mode buffers."
  (setq visual-fill-column-width 100      ; text width
        visual-fill-column-center-text t) ; center text
  (visual-fill-column-mode 1))

(defun org-font-setup ()
  "Sets up fonts for org mode."
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
  (eval-expression (set-face-attribute (car face) nil :font "Ubuntu" :weight 'regular :height (cdr face))))
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                          (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
  (eval-expression (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch))
  (eval-expression (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch)))
  (eval-expression (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch)))
  (eval-expression (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch)))
  (eval-expression (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch)))
  (eval-expression (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch)))
  (eval-expression (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)))

(setq my-org-agenda-files '())

(defun add-org-agenda-file (file)
  "Function to add file to org agenda if it exists."
  (if (file-exists-p file)
      (setq my-org-agenda-files
            (add-to-list 'my-org-agenda-files file))))

;; Add org agenda files from predefined list
(mapcar 'add-org-agenda-file
        '("~/Documents/Projects/Code/Agenda/Tasks.org"
          "~/Documents/Projects/Code/Agenda/Habits.org"
          "~/Documents/Projects/Code/Agenda/Birthdays.org"
          "~/Documents/Projects/Code/Agenda/Events.org"
          "~/Documents/Agenda/Tasks.org"
          "~/Documents/Agenda/Habits.org"
          "~/Documents/Agenda/Birthdays.org"
          "~/Documents/Agenda/Events.org"))

(use-package org
  :pin org                                ; install only from org repository
  :hook (org-mode . org-mode-setup)
  :commands (org-capture org-agenda)
  :custom (org-agenda-files my-org-agenda-files)
  :config
  (setq org-src-preserve-indentation t)   ; respect indentation in src-blocks
  ;; Org Agenda
  (setq org-agenda-start-with-log-mode t
        org-log-done 'time                ; insert timestamp when agenda item done
        org-log-into-drawer t             ; log into attribute drawer
        org-refile-targets                ; files into which to archive old agenda items
        '(("~/Documents/Projects/Code/Agenda/Archive.org" :maxlevel . 1)))
  (advice-add 'org-refile :after 'org-save-all-org-buffers) ; save org buffers after refile
  (require 'org-habit)
  (add-to-list 'org-modules 'org-habit)   ; enable habit tracking
  ;; Org Babel
  (setq org-src-fontify-natively t        ; source block syntax highlighting
        org-confirm-babel-evaluate nil)   ; don't confirm evaluate
  (add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'org-babel-tangle-config)))
  ;; General
  (setq org-return-follows-link t         ; follow link on RET
        org-ellipsis " ▾"                 ; elipsis character to dropdown triangle
        org-hide-emphasis-markers t       ; hide font style characters
        org-clock-sound "~/.emacs.d/bell.wav")
  (if (daemonp)                           ; set org fonts properly if daemon mode
      (add-hook 'after-make-frame-functions
                (lambda (frame)
                  (with-selected-frame frame
                    (org-font-setup))))
    (org-font-setup)))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●"))) ; set custom bullet characters

(use-package org-appear
  :hook (org-mode . org-appear-mode))

(use-package visual-fill-column
  :hook (org-mode . org-mode-visual-fill)) ; centers and formats text

;; Create Org Roam directory if not already present
(unless (file-directory-p "~/Documents/RoamNotes")
  (mkdir "~/Documents/RoamNotes"))

(use-package org-roam
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/Documents/RoamNotes")
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
   '(("d" "default" plain
      "%?"
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
      :unnarrowed t)
     ("m" "piece of music" plain
      (file "~/.emacs.d/templates/org-roam/music.org")
      ;; "\n- Title: ${title}\n- Composer: %^{Composer}\n- Year: %^{Year}\n- Instrumentation: %^{Instrumentation}\n\nNotes:\n%?"
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
      :unnarrowed t)))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         :map org-mode-map
         ("C-M-i" . completion-at-point))
  :config
  (org-roam-setup))

(with-eval-after-load 'org
  (org-babel-do-load-languages
    'org-babel-load-languages
    '((emacs-lisp . t)
        (python . t)
        (lua . t)
        (haskell . t)
        (lisp . t))))

(with-eval-after-load 'org
  (require 'org-tempo)
  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("py" . "src python"))
  (add-to-list 'org-structure-template-alist '("lua" . "src lua"))
  (add-to-list 'org-structure-template-alist '("hs" . "src haskell"))
  (add-to-list 'org-structure-template-alist '("lisp" . "src lisp")))

(defun emms-ffwd ()
  "Fast-forward, random if random playlist set."
  (interactive)
  (if emms-random-playlist
      (emms-random)
    (emms-next)))

(defun track-title-from-file-name (file)
  "For using with EMMS description functions. Extracts the track
title from the file name FILE, which just means a) taking only
the file component at the end of the path, and b) removing any
file extension."
  (with-temp-buffer
    (save-excursion (insert (file-name-nondirectory (directory-file-name file))))
    (ignore-error 'search-failed
      (search-forward-regexp (rx "." (+ alnum) eol))
      (delete-region (match-beginning 0) (match-end 0)))
    (buffer-string)))

(defun my-emms-track-description (track)
  "Return a description of TRACK, for EMMS, but uses
`track-title-from-file-name' to remove directory and extension
from filename in the abscence of metadata information."
  (let ((artist (emms-track-get track 'info-artist))
        (title (emms-track-get track 'info-title)))
    (cond ((and artist title)
           (concat artist " - " title))
          (title title)
          ((eq (emms-track-type track) 'file)
           (track-title-from-file-name (emms-track-name track)))
          (t (emms-track-simple-description track)))))

;; Set up EMMS
(use-package emms
  :commands (emms
             emms-add-file
             emms-add-directory
             emms-add-dired
             emms-play-file
             emms-play-directory
             emms-play-dired)
  :config
  (require 'emms-setup)
  (emms-all)
  (emms-default-players)
  ;; (emms-mode-line-disable) ; Temporary, try to truncate filename first.
  (setq
   emms-source-file-default-directory "~/Music/"
   emms-repeat-playlist t             ; repeat by default
   emms-random-playlist t)            ; shuffle by default
  ;; Media keys to control emms playback
  (global-set-key (kbd "<XF86AudioPlay>") 'emms-pause)
  (global-set-key (kbd "<XF86AudioStop>") 'emms-stop)
  (global-set-key (kbd "<XF86AudioPrev>") 'emms-previous)
  (global-set-key (kbd "<XF86AudioNext>") 'emms-ffwd)
  (setq emms-track-description-function 'my-emms-track-description))

(setq browse-url-browser-function 'eww-browse-url) ; follow links in eww
(eval-after-load 'shr                              ; word wrapping
  '(progn (setq shr-width -1)
          (setq-local visual-line-mode t)
          (defun shr-fill-text (text) text)
          (defun shr-fill-lines (start end) nil)
          (defun shr-fill-line () nil)))

(use-package pdf-tools
  :mode ("\\.pdf\\'" . pdf-view-mode)
  :config
  (unless (pdf-info-check-epdfinfo)
    (pdf-tools-install t)))

(add-to-list 'ispell-local-dictionary-alist '("en_US"
                                              "[[:alpha:]]"
                                              "[^[:alpha:]]"
                                              "[']"
                                              t
                                              ("-d" "en_US")
                                              nil
                                              iso-8859-1))

(setq ispell-program-name "hunspell" ; Use hunspell to correct mistakes
      ispell-dictionary   "en_US")   ; Default dictionary to use

(defun workspace-pdf ()
  "Lays out emacs frame for pdf work."
  (interactive)
  (while (> (eval-expression (count-windows)) 1)
    (delete-window))
  (let* ((wksd (read-directory-name "PDF Workspace Directory: " "~/Documents")))
    ;; Ideal if emacs term would allow -c argument
    (dired (read-directory-name "PDF Workspace Directory: " wksd))
    (split-window-horizontally)       ; split window, showing dired buffer
    (split-window-vertically)         ; split window, showing dired buffer
    (windmove-down)
    (eshell)))                        ; open terminal in bottom left buffer

;; List of packages not otherwise installed.
(defvar package-list '(all-the-icons
                       diminish
                       haskell-mode
                       lua-mode
                       markdown-mode
                       undo-fu)
  "List of packages to be installed.")

;; Add all packages in use-package statements
;; Except those with :ensure nil, already installed with emacs
(setq package-list
      (append package-list
              (mapcar 'intern
                      (split-string
                       (shell-command-to-string "~/.emacs.d/packages.sh")))))

;; Install packages from `package-list'
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

(defun ansi-term-zsh ()
  "Open ansi-term with zsh shell."
  (interactive)
  (if emms-random-playlist
      (emms-random)
    (emms-next)))

(defun force-untabify-all ()
  "Force untabify files that have some read-only text."
  (interactive)
  (let ((inhibit-read-only t)) (untabify-all)))

(defun kill-buffer-delete ()
  "Kill buffer and delete its window or frame."
  (interactive)
  (kill-buffer)
  (delete-window-or-frame))

(defun load-init ()
  "Load default init file."
  (interactive)
  (load-file "~/.emacs.d/init.el"))

(defun menu-music-add-directory ()
  "Add user-chosen music directory."
  (interactive)
  (emms-add-directory-tree (read-directory-name
                            "Add music directory: "
                            emms-source-file-default-directory)))

(defun menu-music-add-file ()
  "Add user-chosen music file."
  (interactive)
  (emms-add-file (read-file-name
                  "Add music file: "
                  emms-source-file-default-directory)))

(defun menu-music-play-directory ()
  "Play user-chosen music directory."
  (interactive)
  (emms-play-directory-tree (read-directory-name
                             "Play music directory: "
                             emms-source-file-default-directory)))

(defun menu-music-play-file ()
  "Play user-chosen music file."
  (interactive)
  (emms-play-file (read-file-name
                   "Play music file: "
                   emms-source-file-default-directory)))

(defun quit-emacs ()
  "Saves buffers and quits emacs."
  (interactive)
  (if (y-or-n-p-with-timeout "Save all buffers and quit emacs?" 5 nil)
      (save-buffers-kill-emacs)))

(defun term-zsh ()
  "Open term with zsh shell."
  (interactive)
  (term "/usr/bin/zsh"))

(use-package hydra
  :defer t)

(defhydra hydra-zoom (:timeout 4)
  "Scale text"
  ("j" zoom-in "zoom in")
  ("k" zoom-out "zoom out")
  ("f" nil "finished" :exit t))

(defhydra hydra-window-resize (:timeout 4)
  "Resize windows"
  ("l" enlarge-window-horizontally "enlarge window horizontally")
  ("h" shrink-window-horizontally "shrink window horizontally")
  ("j" enlarge-window "enlarge window")
  ("k" shrink-window "shrink window")
  ("f" nil "finished" :exit t))

(defhydra hydra-window-move (:timeout 4)
  "Move windows"
  ("l" evil-window-move-far-right "move window right")
  ("h" evil-window-move-far-left "hove window left")
  ("j" evil-window-move-very-bottom "move window down")
  ("k" evil-window-move-very-top "move window up")
  ("f" nil "finished" :exit t))

(defhydra hydra-org-todo (:timeout 4)
  "Toggle org todo items"
  ("t" org-todo "toggle todo state")
  ("f" nil "finished" :exit t))

;; Autocompletion
(use-package auto-complete
  :diminish
  :config
  (ac-config-default))

;; Search/completion functionality
(use-package counsel
  :bind (("M-x" . counsel-M-x)
         ("C-x b" . counsel-ibuffer)
         ("C-x C-f" . counsel-find-file)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history)))

;; Modeline configuration
(use-package doom-modeline
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

;; Theme setting
(use-package doom-themes
  :init (load-theme 'doom-dracula t))

;; Set up EMMS
(use-package emms
  :config
  (require 'emms-setup)
  (emms-all)
  (emms-default-players)
  (emms-mode-line-disable) ; Temporary, try to truncate filename first.
  (setq
   emms-source-file-default-directory "~/Music/"
   emms-repeat-playlist t
   emms-random-playlist t)
  (global-set-key (kbd "<XF86AudioPlay>") 'emms-pause)
  (global-set-key (kbd "<XF86AudioStop>") 'emms-stop)
  (global-set-key (kbd "<XF86AudioPrev>") 'emms-previous)
  (global-set-key (kbd "<XF86AudioNext>") 'emms-ffwd))
;; See above attempt to truncate parent directory
  ;; :custom
  ;; (emms-track-description-function 'currently-playing-track-description)

;; eshell configuration
(use-package eshell
  :hook (eshell-first-time-mode . configure-eshell)
  :config
  (with-eval-after-load 'esh-opt
    (setq eshell-destroy-buffer-when-process-dies t)
    (setq eshell-visual-commands '("htop" "zsh" "vim"))))

;; Evil Configuration
(use-package evil
  :init
  (setq evil-want-keybinding nil)
  (setq evil-undo-system 'undo-fu)
  (setq evil-want-Y-yank-to-eol t)
  (setq evil-want-C-u-delete t)
  (setq evil-want-C-w-delete t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)
  (evil-global-set-key 'motion "/" 'swiper)
  (evil-global-set-key 'motion "SPC" nil)) ; Unbind prefix key

(use-package evil-collection
  :after evil
  :init
  (setq evil-collection-mode-list '(ansi-term
                                    calendar
                                    dired
                                    doc-view
                                    emms
                                    ibuffer
                                    magit
                                    slime
                                    term
                                    vterm)) ; list modes that need evil-collection configuration here
  :config
  (evil-collection-init))

;; Dired Configuration
(add-hook 'find-file-hook 'my-file-find-hook)
(define-key dired-mode-map (kbd " ") nil) ; undefine prefix key
(require 'evil-collection)
(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :custom ((dired-listing-switches "-agho --group-directories-first"))
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "h" 'dired-single-up-directory
    "l" 'dired-open-file
    "f" 'find-file
    " " nil) ; undefine prefix key
  (setq dired-auto-revert-buffer t))

(use-package dired-single
  :commands (dired dired-jump))

;; General keybindings
(use-package general
  :after evil
  :config
  (general-evil-setup t))

(nvmap :prefix "SPC"
  ;; Main menu items
  "b" '(eww :which-key "browser")
  "c" '(comment-dwim :which-key "comment")
  "g" '(magit-status :which-key "magit")
  "h" '(split-window-horizontally :which-key "split horizontally")
  "i" '(ibuffer :which-key "ibuffer")
  "k" '(kill-buffer :which-key "kill buffer")
  "p" '(projectile-command-map :which-key "projectile")
  "q" '(quit-emacs :which-key "quit emacs")
  "r" '(revert-buffer :which-key "revert buffer")
  "s" '(consult-buffer :which-key "switch buffer")
  "u" '(untabify-all :which-key "untabify all")
  "v" '(split-window-vertically :which-key "split vertically")
  "x" '(execute-extended-command :which-key "M-x")
  "z" '(hydra-zoom/body :which-key "zoom")

  ;; Dired items
  "d" '(:ignore t :which-key "dired")
  "dd" '(dired :which-key "dired")
  "dl" '(:ignore t :which-key "directory-local variables")
  "dla" '(add-dir-local-variable :which-key "add directory-local variable")
  "dlc" '(copy-file-locals-to-dir-locals :which-key "copy file-local variables")
  "dld" '(delete-dir-local-variable :which-key "delete directory-local variable")
  "d." '(dired-default :which-key "dired default")

  ;; Eval items
  "e" '(:ignore t :which-key "eval")
  "eb" '(eval-buffer :which-key "eval buffer")
  "ec" '(:ignore t :which-key "common lisp")
  "ecb" '(slime-eval-buffer :which-key "eval common lisp buffer")
  "ecd" '(slime-eval-defun :which-key "eval common lisp defun")
  "ecr" '(slime-eval-region :which-key "eval common lisp region")
  "ecs" '(slime-eval-last-expression :which-key "eval common lisp expression")
  "ed" '(eval-defun :which-key "eval defun")
  "ee" '(eval-expression :which-key "eval expression")
  "ef" '(flymake-show-diagnostics-buffer :which-key "diagnostics")
  "ei" '(load-init :which-key "load init")
  "ep" '(:ignore t :which-key "python")
  "epb" '(python-shell-send-buffer t :which-key "run python buffer")
  "epd" '(python-shell-send-defun t :which-key "run python function")
  "epf" '(python-shell-send-file t :which-key "run python file")
  "epp" '(run-python t :which-key "run python")
  "epr" '(python-shell-send-region t :which-key "run python region")
  "er" '(eval-region :which-key "eval region")
  "es" '(eval-last-sexp :which-key "eval last sexp")

  ;; File items
  "f" '(:ignore t :which-key "file")
  "fe" #'(lambda ()
           (interactive) (find-file (expand-file-name "~/.emacs.d/Emacs.org")))
  "ff" '(find-file :which-key "find file")
  "fl" '(:ignore t :which-key "file-local variables")
  "fla" '(add-file-local-variable :which-key "add file-local variable")
  "flc" '(copy-dir-locals-to-file-locals :which-key "copy directory-local variables")
  "fld" '(delete-file-local-variable :which-key "delete file-local variable")
  "fo" '(find-file-other-window :which-key "find file other window")
  "fw" '(write-file :which-key "write file")

  ;; Music items
  "m" '(:ignore t :which-key "music")
  "ma" '(:ignore t :which-key "add music")
  "mad" '(menu-music-add-directory :which-key "add music directory")
  "maf" '(menu-music-add-file :which-key "add music file")
  "ma." '(emms-add-dired :which-key "add marked music")
  "mp" '(:ignore t :which-key "play music")
  "mpd" '(menu-music-play-directory :which-key "play music directory")
  "mpf" '(menu-music-play-file :which-key "play music file")
  "mp." '(emms-play-dired :which-key "play marked music")
  "mm" '(emms :which-key "emms")
  "mc" '(emms-playlist-current-clear :which-key "clear playlist")
  "mh" '(emms-previous :which-key "previous track")
  "mj" '(emms-stop :which-key "stop")
  "mk" '(emms-pause :which-key "pause")
  "ml" '(emms-ffwd :which-key "next track")
  "mr" '(emms-toggle-repeat-playlist :which-key "toggle repeat")
  "ms" '(emms-toggle-random-playlist :which-key "toggle shuffle")

  ;; Org items
  "o" '(:ignore t :which-key "org")
  "oa" '(org-agenda :which-key "show agenda")
  "ob" '(:ignore t :which-key "org babel")
  "obt" '(org-babel-tangle :which-key "org babel tangle")
  "od" '(org-deadline :which-key "set deadline")
  "oc" '(org-ctrl-c-ctrl-c :which-key "")
  "ol" '(:ignore t :which-key "links")
  "oli" '(org-insert-link :which-key "insert link")
  "olf" '(org-open-at-point :which-key "open link")
  "on" '(:ignore t :which-key "org-roam nodes")
  "onl" '(org-roam-buffer-toggle)
  "onf" '(org-roam-node-find :which-key "find node")
  "oni" '(org-roam-node-insert :which-key "insert node")
  "or" '(org-refile :which-key "refile todo")
  "os" '(org-time-stamp :which-key "set timestamp")
  "ot" '(hydra-org-todo/body :which-key "toggle todo")
  "oT" '(org-timer-set-timer :which-key "set timer")

  ;; Search items
  "a" '(:ignore t :which-key "search")
  "aa" '(consult-apropos :which-key "apropos")
  "aA" '(apropos :which-key "apropos")
  "ac" '(describe-char :which-key "describe character")
  "af" '(describe-function :which-key "describe function")
  "ai" '(info :which-key "manual")
  "ak" '(describe-key :which-key "describe key")
  "am" '(man :which-key "man")
  "aM" '(describe-mode :which-key "describe mode")
  "as" '(describe-symbol :which-key "describe symbol")
  "av" '(describe-variable :which-key "describe variable")

  ;; Term items
  "t" '(:ignore t :which-key "terminals")
  "ta" '(ansi-term-zsh :which-key "zsh ansi-term")
  "te" '(eshell :which-key "eshell")
  "ts" '(shell :which-key "shell")
  "tt" '(term-zsh :which-key "zsh term")
  "tv" '(vterm :which-key "vterm")

  ;; Window items
  "w" '(:ignore t :which-key "window")
  "wc" '(delete-window-or-frame :which-key "close window")
  "wq" '(kill-buffer-delete :which-key "quit window")
  "wh" '(windmove-left :which-key "move to left window")
  "wj" '(windmove-down :which-key "move do lower window")
  "wk" '(windmove-up :which-key "move do upper window")
  "wl" '(windmove-right :which-key "move to right window")
  "wm" '(hydra-window-move/body :which-key "move windows")
  "wr" '(hydra-window-resize/body :which-key "resize windows"))

(nvmap :prefix "z"
  "c" '(vimish-fold-refold :which-key "refold")
  "C" '(vimish-fold-refold-all :which-key "refold all")
  "o" '(vimish-fold-unfold :which-key "unfold")
  "O" '(vimish-fold-unfold-all :which-key "unfold all")
  "a" '(vimish-fold-toggle :which-key "toggle fold")
  "A" '(vimish-fold-toggle-all :which-key "toggle all folds"))

(global-set-key (kbd "<escape>") 'keyboard-escape-quit) ; Bind escape to quit
(global-set-key (kbd "C--") #'(lambda () (interactive) (text-scale-increase -1))) ; Zoom out
(global-set-key (kbd "C-=") #'(lambda () (interactive) (text-scale-increase 1))) ; Zoom in

;; which-key documentation for lambdas
(which-key-add-key-based-replacements "SPC f e" "emacs")

;; (require 'server)
;; (unless (server-running-p)
;;   (server-start))

;; Set garbage collection back to lower, but maintainable, threshold for normal operation
(setq gc-cons-threshold (* 2 1000 1000))
