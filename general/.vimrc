" ~/.vimrc
" config file for the vim text editor

" Lilypond configuration
filetype off
set runtimepath+=/usr/share/vim/vimfiles/syntax/lilypond.vim
filetype on
syntax on

" Misc options
set ttymouse=sgr
set mouse=a
set number
set tabstop=4
set expandtab
set foldmethod=marker

" Keybindings
nmap Y y$
nmap <Space>w <C-W>
imap <C-BS> <C-W>
imap <C-Del> <C-o>de

" Plugin installation for non-arch systems
" Run :PlugInstall on first boot to work
"call plug#begin()
"Plug 'tpope/vim-fugitive'
"Plug 'preservim/nerdtree'
"call plug#end()

func! WordProcessorMode()
    setlocal smartindent
    setlocal spell spelllang=en_us
    setlocal noexpandtab
endfu

com! WP call WordProcessorMode()
